package rest;

import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eclipsesource.json.JsonArray;

import common.Data;
import common.Database;
import scraper.shop.ProductShop;
import server.Rest;
import server.ResultSetConverter;
public class product extends Rest{
	@Public
	public static Object list(Data request) throws Exception {

      	Connection con = null;
      	Statement st = null;
        ResultSet rs = null;
        JSONArray array = null;

        try {
            con = Database.getConnection();
            st = con.createStatement();
   
    		String statement = "SELECT products_shops.*, ts_rank(products_shops.vector, keywords) AS rank, shops.name as shopName FROM products_shops JOIN shops ON products_shops.shop_id = shops.id, to_tsquery('Atlantis') keywords WHERE keywords @@ products_shops.vector ORDER BY products_shops.price DESC";
    		rs = st.executeQuery(statement);
    
    		array = ResultSetConverter.convert(rs);
;
    		System.out.println(array.toString());
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger("Main");
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }

            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger("Main");
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
		return new Data("products", array.toString());
	}
	
	@Public
	public static Object newItems(Data request) throws Exception{
		
		Connection con = null;
      	Statement st = null;
        ResultSet rs = null;
        JSONArray array = null;
        con = Database.getConnection();
        
        try{
            st = con.createStatement();
            String statement = "SELECT products_shops.*, shops.name as shopName FROM products_shops JOIN shops on products_shops.shop_id = shops.id WHERE created > now()::date - 7 ORDER BY created DESC";
    		System.out.println(statement);
    		rs = st.executeQuery(statement);
    	    
    		array = ResultSetConverter.convert(rs);
        }catch (SQLException ex) {
            Logger lgr = Logger.getLogger("Main");
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }

            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger("Main");
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
		return new Data("products", array.toString());
	}
	
	@Public
	public static Object search(Data request) throws Exception{	
		Connection con = null;
      	Statement st = null;
        ResultSet rs = null;
        JSONArray array = null;
        String search = request.getString("search");
        con = Database.getConnection();
        
        try{
            st = con.createStatement();
        //  search = search.replace("'", "").replace(" ", " | ");
     //       System.out.println("search "+ search);
            search = search.replace("'", "''");
            String statement = "SELECT products_shops.*, ts_rank(products_shops.vector, keywords) AS rank, shops.name as shopName FROM products_shops JOIN shops ON products_shops.shop_id = shops.id, plainto_tsquery('"+search+"') keywords WHERE keywords @@ products_shops.vector ORDER BY price ASC";
    		System.out.println(statement);
    		rs = st.executeQuery(statement);
    	    
    		array = ResultSetConverter.convert(rs);
        }catch (SQLException ex) {
            Logger lgr = Logger.getLogger("Main");
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }

            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger("Main");
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
		return new Data("products", array.toString());
	}
	
	
	@Public
	public static Object unassigned(Data request) throws Exception {
		Connection con = null;
      	Statement st = null;
        ResultSet rs = null;
        JSONArray array = null;

        try {
            con = Database.getConnection();
            st = con.createStatement();
   
    		String statement = "SELECT * FROM products_shops WHERE product_id IS NULL";
    		rs = st.executeQuery(statement);
    	    
    		array = ResultSetConverter.convert(rs);
        }catch (SQLException ex) {
            Logger lgr = Logger.getLogger("Main");
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }

            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger("Main");
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
		return new Data("products", array.toString());
	}
}
