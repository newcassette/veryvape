package rest;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;

import common.Data;
import common.Database;
import server.Rest;
import server.ResultSetConverter;
public class brand extends Rest{
	@Public
	public static Object list(Data request) throws Exception {

		Connection con = null;
      	Statement st = null;
        ResultSet rs = null;
        JSONArray array = null;

        try {
            con = Database.getConnection();
            st = con.createStatement();
   
    		String statement = "SELECT * FROM brands";
    		rs = st.executeQuery(statement);
    		con.close();
    		System.out.println(rs.toString());
    		array = ResultSetConverter.convert(rs);

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger("Main");
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }

            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger("Main");
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
		return new Data("brands", array.toString());
	}
}