package server;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import org.eclipse.jetty.server.*;
import org.eclipse.jetty.server.handler.*;
import org.eclipse.jetty.servlet.*;
import org.mvel2.integration.impl.MapVariableResolverFactory;
import org.mvel2.sh.ShellSession;

import static org.mvel2.MVEL.compileExpression;
import static org.mvel2.MVEL.executeExpression;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

public class Main {
	static ShellSession shell;
	static MapVariableResolverFactory lvrf;
	
	@SuppressWarnings("unchecked")
	static JsonValue toJson(Object value) {
		if (value instanceof Integer) {
			return JsonValue.valueOf((Integer)value);
		} else if (value instanceof Double) {
			return JsonValue.valueOf((Double)value);
		} else if (value instanceof String) {
			return JsonValue.valueOf((String)value);
		} else if (value instanceof Boolean) {
			return JsonValue.valueOf((Boolean)value);
		} else if (value instanceof Map<?, ?>) {
			JsonObject object = new JsonObject();
			for (Map.Entry<String, Object> entry : ((Map<String, Object>)value).entrySet()) {
				object.add(entry.getKey(), toJson(entry.getValue()));
			}
			return object;
		} else if (value instanceof List<?>) {
			JsonArray array = new JsonArray();
			for (Object element : (List<Object>)value) {
				array.add(toJson(element));
			}
			return array;
		} else {
			return JsonValue.NULL;
		}
	}
	
	public static void load(String fileName) throws IOException {
		byte[] bytes = Files.readAllBytes(Paths.get(fileName));
		String code = new String(bytes, StandardCharsets.UTF_8);
		executeExpression(compileExpression(code), shell.getCtxObject(), lvrf);
	}
	
	public static void exit() {
		System.exit(0);
	}
	
	public static void main(String[] args) {
		Server server = new Server(8008);
		HandlerList handlers = new HandlerList();
		ServletContextHandler contextHandler = new ServletContextHandler();
		contextHandler.setContextPath("/");
		ResourceHandler resourceHandler = new ResourceHandler();
	    resourceHandler.setDirectoriesListed(true);
	    resourceHandler.setWelcomeFiles(new String[]{"index.xhtml"});
	    resourceHandler.setResourceBase("./WebContent");
	    resourceHandler.setMinMemoryMappedContentLength(-1);
	    handlers.addHandler(resourceHandler);
	    handlers.addHandler(contextHandler);
		server.setHandler(handlers);
		contextHandler.addServlet(new ServletHolder(new ExecHandler()), "/exec");
		try {
			server.start();
			server.join();
		} catch (Exception error) {
			error.printStackTrace();
		}
	}
}
