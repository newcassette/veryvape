package server;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONValue;

import common.Array;
import common.Data;

public class ExecHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		Object result;
		try {
			Array query = Array.readFrom(request.getReader());
			result = Rest.call(query.getString(0), query.getObject(1));
			System.out.println(query.getString(0));
			//result = "{'obj': 'value'}";
		} catch (Exception error) {
			error.printStackTrace();
			response.setStatus(500);
			result = new Data().set("error", "Internal error");
		}
		JSONValue.writeJSONString(result, response.getWriter());
	}
}
