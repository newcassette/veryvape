package server;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import common.Data;

public class Rest {
	@Retention(RetentionPolicy.RUNTIME)
	@Target({ElementType.METHOD})
	public @interface Public {
	}

	public static class Error extends Exception {
		private static final long serialVersionUID = 1L;

		public String message;

		public Error() {
			this.message = "Unknown error";
		}

		public Error(String message) {
			this.message = message;
		}
	}
	
	
	private static final Map<String, Method> methods = new HashMap<>();
	private static final Set<String> keywords = new HashSet<>();
	
	public static Object call(String methodName0, Data request) throws Exception {
		Method method = methods.computeIfAbsent(methodName0, methodName -> {
			String names[] = methodName.split("/");
			try {
				if (names.length < 2) return null;
				StringBuilder builder = new StringBuilder();
				builder.append("rest.");
				builder.append(names[0]);
				for (int i = 1; i < names.length - 1; ++i) {
					String name = names[i];
					builder.append("$");
					if (keywords.contains(name)) builder.append("_"); 
					builder.append(name);
				}
				String className = builder.toString();
				Class<?> _class = Class.forName(className);
				Method _method = _class.getDeclaredMethod(names[names.length - 1], Data.class);
				System.out.println(className+" ");
				if (_method.getAnnotation(Public.class) != null) return _method;
			} catch (ClassNotFoundException error) {
				error.printStackTrace();
			} catch (NoSuchMethodException error) {
				error.printStackTrace();
			}
			return null;
		});
		if (method == null) throw new Exception();
		try {
			return method.invoke(null, request);
		} catch (Exception error) {
			error.printStackTrace();
			throw new Error("Internal error");
		}
	}
	
}
