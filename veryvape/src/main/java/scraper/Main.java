package scraper;

import java.beans.PropertyVetoException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.money.convert.CurrencyConversion;
import javax.money.convert.MonetaryConversions;
import javax.naming.spi.DirStateFactory.Result;
import javax.print.attribute.standard.PresentationDirection;

import scraper.shop.ProductShop;
import scraper.shop.Shop;
import scraper.shop.ireland.BargainVapour;
import scraper.shop.ireland.Cigatech;
import scraper.shop.ireland.ECigShop;
import scraper.shop.ireland.ECigarette_;
import scraper.shop.ireland.ECirette_;
import scraper.shop.ireland.EFag;
import scraper.shop.ireland.ESmokeIreland;
import scraper.shop.ireland.EZSmoke;
import scraper.shop.ireland.IrishVape;
import scraper.shop.ireland.PurpleBox;
import scraper.shop.ireland.SAndHeaven;
import scraper.shop.ireland.SmokeDifferent;
import scraper.shop.ireland.TheBestShop;
import scraper.shop.ireland.TheStopSmokingShop;
import scraper.shop.ireland.VapeFreaks;
import scraper.shop.ireland.Vaperus;
import scraper.shop.ireland.Vaporium;
import scraper.shop.ireland.VapourIreland;
import scraper.shop.ireland.VapourPal;
import scraper.shop.usa.OneZeroOneVape;
import scraper.shop.usa.ThreeFVape;
import scraper.shop.usa.TwoVaped;

import org.javamoney.moneta.Money;

import common.DataSource;

public class Main {

	private static double USD;
	private static double GBP;
	
	public static void main(String[] args) throws IOException, PropertyVetoException, ClassNotFoundException {
		//getCurrencies();
		parseIreland();
		//parseUk();
		//parseUSA();
	}
	
	private static void getCurrencies(){
		MonetaryAmount dollarAmount = Money.of(1, "USD");
		MonetaryAmount gbpAmount = Money.of(1, "GBP");
		USD = dollarAmount.with(MonetaryConversions.getConversion("EUR", "ECB")).getNumber().doubleValue();
		GBP = gbpAmount.with(MonetaryConversions.getConversion("EUR", "ECB")).getNumber().doubleValue();
	}
	
	private static void parseUk(){
		ArrayList<Shop> shops = new ArrayList<Shop>();
		
	}
	
	private static void parseUSA() {
		ArrayList<Shop> shops = new ArrayList<Shop>();
		
		//shops.add(new OneZeroOneVape());
		//shops.add(new TwoVaped());
		shops.add(new ThreeFVape());
		
		for(Shop shop: shops){
			ArrayList<ProductShop> products = shop.getProducts();
			//if(products!=null)saveToDatabase(products, USD);
		}
	}
	
	private static void parseIreland() throws IOException, PropertyVetoException, ClassNotFoundException{
		ArrayList<Shop> shops = new ArrayList<Shop>();
		shops.add(new BargainVapour());
		shops.add(new EFag());
		shops.add(new ESmokeIreland());
		shops.add(new EZSmoke());
		shops.add(new IrishVape());
		shops.add(new PurpleBox());
		shops.add(new SAndHeaven());
		shops.add(new TheBestShop());
		shops.add(new VapeFreaks());
		shops.add(new Vaporium());
		shops.add(new VapourIreland());
		shops.add(new VapourPal());
	    shops.add(new Cigatech());
		shops.add(new TheStopSmokingShop());
		shops.add(new SmokeDifferent());
		
		for(Shop shop: shops){
			ArrayList<ProductShop> products = shop.getProducts();
			if(products!=null)saveToDatabase(products, 1);
		}
	}
	
	public static void saveToDatabase(ArrayList<ProductShop> products, double exchange) throws IOException, PropertyVetoException, ClassNotFoundException {	
      	Connection con = null;
      	Statement st = null;
        try {
        	con = DataSource.getInstance().getConnection();
            st = con.createStatement();         
            for (ProductShop p: products) {
            	if(p != null){
	        	//	p.name = p.name.replace("'"," ");
	        		String selectStatement = "SELECT id as product_id FROM products_shops WHERE (name = ? AND shop_id = ?)";
	        		PreparedStatement preparedStatement = con.prepareStatement(selectStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
	        		preparedStatement.setString(1, p.name);
	        		preparedStatement.setInt(2, p.shop);        		
	        		ResultSet rs = preparedStatement.executeQuery();
	        		Calendar cal = Calendar.getInstance();
	        		if(rs.first()){
	        			String updateStatement = "UPDATE products_shops SET price = ?, updated = ? WHERE id = ?";
	        			PreparedStatement preparedStatement1 = con.prepareStatement(updateStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
	        			preparedStatement1.setDouble(1, p.price);
	        			preparedStatement1.setInt(3, rs.getInt("product_id"));
	        			preparedStatement1.setDate(2, new Date(cal.getTimeInMillis()));
	        			preparedStatement1.executeUpdate();
	        			System.out.println("update performed on product "+rs.getInt("product_id"));
	        		}else {
	        			String insertStatement = "INSERT INTO products_shops(name, price, shop_id, link, list_image, vector, created, updated) VALUES (?,?,?,?,?,to_tsvector(?),?,?)";
	        			PreparedStatement preparedStatement1 = con.prepareStatement(insertStatement, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
	        			preparedStatement1.setString(1, p.name);
	        			preparedStatement1.setDouble(2, p.price);
	        			preparedStatement1.setInt(3, p.shop);
	        			preparedStatement1.setString(4, p.link);
	        			preparedStatement1.setString(5, p.imageLink);
	        			preparedStatement1.setString(6, p.name);
	        			preparedStatement1.setDate(7, new Date(cal.getTimeInMillis()));
	        			preparedStatement1.setDate(8, new Date(cal.getTimeInMillis()));
	        			preparedStatement1.execute();
	        			System.out.println(preparedStatement1.toString());
	        		}
            	}
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger("Main");
            lgr.log(Level.SEVERE, ex.getMessage(), ex);           
        } finally {
            try {
                if (st != null) st.close();              
                if (con != null) con.close();               
            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger("Main");
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
	}
	
	public static void saveImage(String imageUrl, String destinationFile) throws IOException {
		URL url = new URL(imageUrl);
		InputStream is = url.openStream();
		OutputStream os = new FileOutputStream(destinationFile);
		byte[] b = new byte[2048];
		int length;
		while ((length = is.read(b)) != -1) os.write(b, 0, length);
		is.close();
		os.close();
	}
}
