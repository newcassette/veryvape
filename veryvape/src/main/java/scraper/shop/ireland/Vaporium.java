package scraper.shop.ireland;

//COME BACK TO THIS

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;



public class Vaporium implements Shop{
	public ArrayList<Link> links;
	private final int id = 13;
	public Vaporium() {
		links = new ArrayList<Link>();	
		links.add(new Link("http://www.vaporium.eu/5-starter-kits", id, Link.C_ACCESSORIES));
		links.add(new Link("http://www.vaporium.eu/5-starter-kits", id, Link.C_STARTER_KITS));
		links.add(new Link("http://www.vaporium.eu/8-standard-e-liquid", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/11-pink-spot", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/29-flavorz-by-joe", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/32-villain-vapors", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/37-the-standard-vape", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/38-jameson-s-irish-vapor-juice", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/47-30ml-bottles", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/46-15ml-bottles", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/40-ben-johnson-s-awesome-sauce", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/41-juice-by-numbers", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/48-15ml", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/49-30ml-", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/43-mastermind-elixirs", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/44-faded-vapes", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/51-blueprint", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/53-namber-juice", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/54-witchers-brew", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/56-ruckus-elixirs", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/57-epic-juice", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/58-adam-bomb", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/60-mad-murdock", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/66-15ml-bottles", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/67-30ml-", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/68-lost-art-liquids-", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/73-the-juice-society", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/74-rocket-sheep-juice", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/75-han-sauce", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/76-the-steam-factory", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/77-pure-lightning-", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/79-vaping-monkey", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/81-macsauce", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/85-epiclouds-by-namber-juice", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/86-high-roller-sweets", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/90-tugsauce", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/91-the-best-damn-e-juice", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/92-juicy-ohms", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/93-the-schwartz", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaporium.eu/82-beyond-vape", id, Link.C_RBA));
		links.add(new Link("http://www.vaporium.eu/84-standard-functions", id, Link.C_MECH_MODS));
		links.add(new Link("http://www.vaporium.eu/80-vapor-shark", id, Link.C_REGULATED_MODS));
		links.add(new Link("http://www.vaporium.eu/62-style-of-mojo", id, Link.C_MECH_MODS));
		links.add(new Link("http://www.vaporium.eu/88-science-of-vaping", id, Link.C_RBA));
		links.add(new Link("http://www.vaporium.eu/63-zenith-v2", id, Link.C_ACCESSORIES));
		links.add(new Link("http://www.vaporium.eu/31-joyetech-evic", id, Link.C_MECH_MODS));
		links.add(new Link("http://www.vaporium.eu/34-innokin", id, Link.C_REGULATED_MODS));
		links.add(new Link("http://www.vaporium.eu/9-cartomizers", id, Link.C_CARTOMIZER));
		links.add(new Link("http://www.vaporium.eu/12-tanks-clearomizers", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://www.vaporium.eu/13-batteries", id, Link.C_BATTERIES));
		links.add(new Link("http://www.vaporium.eu/14-chargers", id, Link.C_CHARGERS));
		links.add(new Link("http://www.vaporium.eu/15-drip-tips", id, Link.C_DRIP_TIPS));
		links.add(new Link("http://www.vaporium.eu/16-acessories", id, Link.C_ACCESSORIES));
		links.add(new Link("http://www.vaporium.eu/35-rebuildables", id, Link.C_RBA));
		links.add(new Link("http://www.vaporium.eu/36-mechanical-mods", id, Link.C_RBA));
		links.add(new Link("http://www.vaporium.eu/78-abstract-vape", id, Link.C_ACCESSORIES));
	}
	
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		for(Link link : links) {
			try {
				Document doc = Jsoup.connect(link.url).timeout(10000).get();

				Elements productContainers = doc.getElementById("product_list").getElementsByClass("ajax_block_product");
				for(int i = 0; i < productContainers.size(); i++) {
					products.add(getProduct(productContainers.get(i), link.category));
				}
				
			}catch(IOException e){
				e.printStackTrace();
			}
		}
		return products;
	}
	
	private ProductShop getProduct(Element element, int cat) {		
		
			ProductShop p = new ProductShop();
			p.link = element.getElementsByClass("product_img_link").attr("href");
			p.name = element.getElementsByClass("product_img_link").attr("title");
			p.price = Double.valueOf(element.getElementsByClass("price").text().replace(",", ".").replace("�", " ").trim());
			p.shop = id;
			p.imageLink = element.getElementsByClass("product_img_link").select("img").get(0).attr("src");
			p.category = cat;
			p.shop = id;
			System.out.println(p.toString());
			return p;	
	}

	public ArrayList<Link> getLinks() {
		// TODO Auto-generated method stub
		return links;
	}

}
