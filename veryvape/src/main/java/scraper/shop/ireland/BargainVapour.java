package scraper.shop.ireland;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;

public class BargainVapour implements Shop {

	
	ArrayList<Link> links;
	private final int id = 4;
	public BargainVapour() {
		links = new ArrayList<Link>();
		
		links.add(new Link("http://www.bargainvapour.com/The_100ml_sale!/cat1274300_1558093.aspx", id, Link.C_EJUICE));
		links.add(new Link("http://www.bargainvapour.com/index.aspx?pageid=1274300&category=1558093&Page=2", id, Link.C_EJUICE));
		links.add(new Link("http://www.bargainvapour.com/index.aspx?pageid=1274300&category=1558093&Page=3", id, Link.C_EJUICE));
		links.add(new Link("http://www.bargainvapour.com/Starter_Kits/cat1274300_1157996.aspx", id, Link.C_STARTER_KITS));
		links.add(new Link("http://www.bargainvapour.com/Aspire/cat1274300_2536009.aspx", id, Link.C_REGULATED_MODS));
		links.add(new Link("http://www.bargainvapour.com/Biansi/cat1274300_1381746.aspx", id, Link.C_CIGALIKES));
	//	links.add(new Link("http://www.bargainvapour.com/Evolv/cat1274300_1409896.aspx", id, Link.C_ACCESSORIES));
		links.add(new Link("http://www.bargainvapour.com/Kanger/cat1274300_2198332.aspx", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://www.bargainvapour.com/JoyeTech/cat1274300_1690660.aspx", id, Link.C_COILS));
		links.add(new Link("http://www.bargainvapour.com/SmokTech/cat1274300_2198476.aspx", id, Link.C_REGULATED_MODS));
		links.add(new Link("http://www.bargainvapour.com/Vision/cat1274300_2402312.aspx", id, Link.C_REGULATED_MODS));
		links.add(new Link("http://www.bargainvapour.com/Mods/cat1274300_1571250.aspx", id, Link.C_REGULATED_MODS));
		links.add(new Link("http://www.bargainvapour.com/index.aspx?pageid=1274300&category=1571250&Page=2", id, Link.C_REGULATED_MODS));
		links.add(new Link("http://www.bargainvapour.com/Samples/cat1274300_1997805.aspx", id, Link.C_EJUICE));
		links.add(new Link("http://www.bargainvapour.com/Hangsen/cat1274300_1456563.aspx", id, Link.C_EJUICE));
		links.add(new Link("http://www.bargainvapour.com/index.aspx?pageid=1274300&category=1456563&Page=2", id, Link.C_EJUICE));
		links.add(new Link("http://www.bargainvapour.com/index.aspx?pageid=1274300&category=1456563&Page=3", id, Link.C_EJUICE));
		links.add(new Link("http://www.bargainvapour.com/Hangsen_VG/cat1274300_2373153.aspx", id, Link.C_EJUICE));
		links.add(new Link("http://www.bargainvapour.com/Texas_Select_USA/cat1274300_1521867.aspx", id, Link.C_EJUICE));
		links.add(new Link("http://www.bargainvapour.com/DKS_100__Natural/cat1274300_1445598.aspx", id, Link.C_EJUICE));
		links.add(new Link("http://www.bargainvapour.com/Small__amp;_Stylish/cat1274300_1476370.aspx", id, Link.C_CIGALIKES));
		links.add(new Link("http://www.bargainvapour.com/Atomizers__amp;_Wicks/cat1274300_1212931.aspx", id, Link.C_COILS));
		links.add(new Link("http://www.bargainvapour.com/Re-buildable/cat1274300_2238240.aspx", id, Link.C_RBA));
		links.add(new Link("http://www.bargainvapour.com/Tanks__amp;_Cartridges/cat1274300_1318813.aspx", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://www.bargainvapour.com/index.aspx?pageid=1274300&category=1318813&Page=2", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://www.bargainvapour.com/Batteries/cat1274300_1212924.aspx", id, Link.C_BATTERIES));
		links.add(new Link("http://www.bargainvapour.com/index.aspx?pageid=1274300&category=1212924&Page=2", id, Link.C_BATTERIES));
		links.add(new Link("http://www.bargainvapour.com/VV_Batteries/cat1274300_2003246.aspx", id, Link.C_CIGALIKES));
		links.add(new Link("http://www.bargainvapour.com/Cartomizers/cat1274300_1254442.aspx", id, Link.C_CIGALIKES));
		links.add(new Link("http://www.bargainvapour.com/Chargers/cat1274300_1157997.aspx", id, Link.C_CHARGERS));
		links.add(new Link("http://www.bargainvapour.com/DIY__amp;_Mixing/cat1274300_1332142.aspx", id, Link.C_DIY));
		links.add(new Link("http://www.bargainvapour.com/index.aspx?pageid=1274300&category=1332142&Page=2", id, Link.C_DIY));
		links.add(new Link("http://www.bargainvapour.com/Accessories/cat1274300_1267297.aspx", id, Link.C_ACCESSORIES));
		links.add(new Link("http://www.bargainvapour.com/Drip_Tips/cat1274300_1380269.aspx", id, Link.C_ACCESSORIES));
	}
	
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		for(Link link : links) {
			try {
				Document doc = Jsoup.connect(link.url).timeout(10000).get();
				Elements productElements = doc.getElementsByClass("prodbox");
				for(Element e : productElements) products.add(getProduct(e, link.category));
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
		return products;
	}
	
	private ProductShop getProduct(Element e, int category){
		ProductShop p = new ProductShop();	
		Element linkAndTitleContainer = e.getElementsByClass("prodbox_title").get(0);
		Element a = linkAndTitleContainer.select("a").get(0);
		String img = e.getElementsByClass("prodbox_image").get(0).select("img").get(0).attr("src");
		String link = a.attr("href");
		String name = a.text();
		p.link = link;
		p.name = name;		
		p.imageLink = img;
		p.category = category;
		Element priceContainer = e.getElementsByClass("prodbox_price").get(0);
		String priceStr = priceContainer.text();
		String dPrice = (priceStr.replace(",", ".").substring(0, priceStr.length()-1).trim());
		if(dPrice.contains(" "))dPrice = dPrice.split(" ")[1];
		Double price = Double.valueOf(dPrice);
		p.price = price;
		p.shop = id;
		System.out.println(p.toString());
		return p;
	}

	public ArrayList<Link> getLinks() {
		return links;
	}


	
}
