package scraper.shop.ireland;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;



public class VapeFreaks implements Shop{
	public ArrayList<Link> links;
	private final int id = 11;
	public VapeFreaks() {
		links = new ArrayList<Link>();	
		links.add(new Link("http://www.vapefreaks.com/100ml_E_Liquid/cat2486622_2229703.aspx", id, Link.C_EJUICE));	
		links.add(new Link("http://www.vapefreaks.com/Sample_E_Liquid/cat2486622_2264910.aspx", id, Link.C_EJUICE));
		links.add(new Link("http://www.vapefreaks.com/Diablo_E_Liquid/cat2486622_2776162.aspx", id, Link.C_EJUICE));
		links.add(new Link("http://www.vapefreaks.com/100__VG_E_Liquid/cat2486622_2728996.aspx", id, Link.C_EJUICE));
		links.add(new Link("http://www.vapefreaks.com/30ml__Only__399/cat2486622_2580955.aspx", id, Link.C_EJUICE));
		links.add(new Link("http://www.vapefreaks.com/Clearomizers/cat2486622_2230557.aspx", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://www.vapefreaks.com/Coils/cat2486622_2556445.aspx", id, Link.C_COILS));
		links.add(new Link("http://www.vapefreaks.com/Batteries/cat2486622_2227872.aspx", id, Link.C_BATTERIES));
		links.add(new Link("http://www.vapefreaks.com/Mods/cat2486622_2251866.aspx", id, Link.C_MECH_MODS));
		links.add(new Link("http://www.vapefreaks.com/Starter_Kits/cat2486622_2227871.aspx", id, Link.C_STARTER_KITS));
		links.add(new Link("http://www.vapefreaks.com/Chargers/cat2486622_2556436.aspx", id, Link.C_CHARGERS));
		links.add(new Link("http://www.vapefreaks.com/DIY__amp;_Mixing/cat2486622_2515728.aspx", id, Link.C_DIY));
		links.add(new Link("http://www.vapefreaks.com/Accessories/cat2486622_2229708.aspx", id, Link.C_ACCESSORIES));
	}
	
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		for(Link link : links) {
			try {
				Document doc = Jsoup.connect(link.url).timeout(10000).get();

				Elements productContainers = doc.getElementsByClass("pricing-table");
				for(int i = 0; i < productContainers.size(); i++) {
					products.add(getProduct(productContainers.get(i), link.category));
				}
				
			}catch(IOException e){
				e.printStackTrace();
			}
		}
		return products;
	}
	
	private ProductShop getProduct(Element element, int cat) {		
		ProductShop p = new ProductShop();	
		p.link = element.getElementsByClass("title").select("a").attr("href");
		p.name = element.getElementsByClass("title").select("a").text();
		p.price = Double.valueOf(element.getElementsByClass("price").text().replace("�", " ").replace(",", ".").trim());
		p.shop = id;
		p.imageLink = element.getElementsByClass("cta-button").select("img").get(0).attr("src");
		p.category = cat;
		System.out.println(p.toString());
		return p;
	}

	public ArrayList<Link> getLinks() {
		return links;
	}

}
