package scraper.shop.ireland;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;

public class ECirette_ implements Shop{

	ArrayList<Link> links;
	private final int id = 5;
	
	
	public ECirette_(){
		links = new ArrayList<Link>();
		links.add(new Link("http://ecirette.ie/product-category/e-liquid/", id, Link.C_EJUICE));
		links.add(new Link("http://ecirette.ie/product-category/e-liquid/page/2/", id, Link.C_EJUICE));
		links.add(new Link("http://ecirette.ie/product-category/abstract-vape/", id, Link.C_ACCESSORIES));
		links.add(new Link("http://ecirette.ie/product-category/accessories/", id, Link.C_ACCESSORIES));
		links.add(new Link("http://ecirette.ie/product-category/accessories/page/2/", id, Link.C_ACCESSORIES));
	}
	
	@Override
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		for(Link link : links) {
			try {
				Document doc = Jsoup.connect(link.url).userAgent("Chrome").timeout(10000).get();
				System.out.println(doc);
				Elements productElements = doc.getElementsByClass("product_item");
				for(Element e : productElements) products.add(getProduct(e));
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public ProductShop getProduct(Element e){
		
		System.out.println(e.toString());
		return null;
	}

	@Override
	public ArrayList<Link> getLinks() {
		// TODO Auto-generated method stub
		return null;
	}

}
