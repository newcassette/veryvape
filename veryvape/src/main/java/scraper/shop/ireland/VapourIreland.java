package scraper.shop.ireland;

//COME BACK TO THIS

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;



public class VapourIreland implements Shop{
	public ArrayList<Link> links;
	private final int id = 14;
	public VapourIreland() {
		links = new ArrayList<Link>();	
		links.add(new Link("http://www.vapourireland.com/e-cigarette-starter-kits.html", id,Link.C_STARTER_KITS));
		links.add(new Link("http://www.vapourireland.com/kanger-evod.html", id, Link.C_CIGALIKES));
		links.add(new Link("http://www.vapourireland.com/joye-ego-t.html", id, Link.C_CIGALIKES));
		links.add(new Link("http://www.vapourireland.com/joye-ecab.html", id, Link.C_BATTERIES));
		links.add(new Link("http://www.vapourireland.com/e-liquid.html", id, Link.C_EJUICE));
		links.add(new Link("http://www.vapourireland.com/joye-ego-c.html", id, Link.C_CIGALIKES));
		links.add(new Link("http://www.vapourireland.com/accessories.html", id, Link.C_ACCESSORIES));
		links.add(new Link("http://www.vapourireland.com/clearomizers.html", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://www.vapourireland.com/atomizers.html", id, Link.C_CIGALIKES));
		links.add(new Link("http://www.vapourireland.com/batteries.html", id, Link.C_CIGALIKES));
		links.add(new Link("http://www.vapourireland.com/cartomizers.html", id, Link.C_CARTOMIZER));
		links.add(new Link("http://www.vapourireland.com/cartridges.html", id, Link.C_CIGALIKES));
		links.add(new Link("http://www.vapourireland.com/chargers.html", id, Link.C_CHARGERS));
	}
	
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		
		for (Link link : links) {
			try {
				Document doc = Jsoup.connect(link.url).timeout(10000).get();
				Elements productContainers = doc.getElementsByClass("category_product");
				for(int i = 0; i < productContainers.size(); i++) {
					products.add(getProduct(productContainers.get(i), link.category));
				}
				
			}catch(IOException e){
				e.printStackTrace();
			}
		}
		return products;
	}
	
	private ProductShop getProduct(Element element, int cat) {		
			ProductShop p = new ProductShop();
			p.link = element.select("h3").select("a").attr("href");
			p.name = element.select("h3").select("a").attr("title");
			p.price = Double.valueOf(element.getElementsByClass("price_sale").get(0).text().replace(",", ".").replace("�", " ").trim());
			p.shop = id;
			p.imageLink = element.select("img").get(0).attr("src");
			p.category = cat;
			System.out.println(p.toString());
			return p;	
	}

	public ArrayList<Link> getLinks() {
		// TODO Auto-generated method stub
		return links;
	}

}
