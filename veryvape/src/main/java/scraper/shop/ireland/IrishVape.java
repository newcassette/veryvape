package scraper.shop.ireland;

import java.io.IOException;
import java.util.ArrayList;








import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;

public class IrishVape implements Shop{

	ArrayList<Link> links;
	private final int id = 7;
	
	
	public IrishVape() {
		links = new ArrayList<Link>();
		links.add(new Link("http://irishvape.com/tanks-clearomizers/", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://irishvape.com/standard-kits/", id, Link.C_CIGALIKES));
		links.add(new Link("http://irishvape.com/advanced-kits-2/", id, Link.C_REGULATED_MODS));
		links.add(new Link("http://irishvape.com/batteries/variable/", id, Link.C_REGULATED_MODS));
		links.add(new Link("http://irishvape.com/mods/box-mods/", id, Link.C_REGULATED_MODS));
		links.add(new Link("http://irishvape.com/mods/mech-mods/", id, Link.C_MECH_MODS));
		links.add(new Link("http://irishvape.com/mods/ecigarettes-mods/provari/", id, Link.C_MECH_MODS));
		links.add(new Link("http://irishvape.com/mods/rebuildable/tanksrtas/", id, Link.C_RBA));
		links.add(new Link("http://irishvape.com/rebuildable-atomizers/sub-tanks/", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://irishvape.com/rebuildable-atomizers/drippersrdas-2/", id, Link.C_RBA));
		links.add(new Link("http://irishvape.com/kanthol/", id, Link.C_DIY));
		links.add(new Link("http://irishvape.com/mesh/", id, Link.C_DIY));
		links.add(new Link("http://irishvape.com/diy/mod-batteries/", id, Link.C_BATTERIES));
		links.add(new Link("http://irishvape.com/diy/wicking/", id, Link.C_DIY));
		links.add(new Link("http://irishvape.com/tanks-clearomizers/", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://irishvape.com/coils/", id, Link.C_COILS));
		links.add(new Link("http://irishvape.com/drip-tips/", id, Link.C_DRIP_TIPS));
		links.add(new Link("http://irishvape.com/chargers-car-chargers-passthroughs/", id, Link.C_CHARGERS));
		links.add(new Link("http://irishvape.com/cases/", id, Link.C_ACCESSORIES));
		links.add(new Link("http://irishvape.com/holders/", id, Link.C_ACCESSORIES));
		links.add(new Link("http://irishvape.com/usa-e-juice/beard-vape-co/", id, Link.C_EJUICE));
		links.add(new Link("http://irishvape.com/usa-e-juice/fuzion-vapors/", id, Link.C_EJUICE));
		links.add(new Link("http://irishvape.com/usa-e-juice/halo/", id, Link.C_EJUICE));
		links.add(new Link("http://irishvape.com/usa-e-juice/pipe-sauce/", id, Link.C_EJUICE));
		links.add(new Link("http://irishvape.com/usa-e-juice/e-liquid/artist-collection/", id, Link.C_EJUICE));
		links.add(new Link("http://irishvape.com/menthol-flavours/", id, Link.C_EJUICE));
		links.add(new Link("http://irishvape.com/usa-e-juice/chapter-xii/", id, Link.C_EJUICE));
		links.add(new Link("http://irishvape.com/usa-e-juice/g2-vapors/", id, Link.C_EJUICE));
		links.add(new Link("http://irishvape.com/usa-e-juice/innevape/", id, Link.C_EJUICE));
		links.add(new Link("http://irishvape.com/usa-e-juice/ripe-vapes/", id, Link.C_EJUICE));
		links.add(new Link("http://irishvape.com/usa-e-juice/the-standard/", id, Link.C_EJUICE));
		links.add(new Link("http://irishvape.com/usa-e-juice/cosmic-fog-vapors/", id, Link.C_EJUICE));
		links.add(new Link("http://irishvape.com/usa-e-juice/grand-vapors/", id, Link.C_EJUICE));
		links.add(new Link("http://irishvape.com/usa-e-juice/kings-crown/", id, Link.C_EJUICE));
		links.add(new Link("http://irishvape.com/usa-e-juice/space-jam/", id, Link.C_EJUICE));
		links.add(new Link("http://irishvape.com/usa-e-juice/velvet-cloud/", id, Link.C_EJUICE));
		links.add(new Link("http://irishvape.com/usa-e-juice/cuttwood/", id, Link.C_EJUICE));
		links.add(new Link("http://irishvape.com/usa-e-juice/halcyon-vapors/", id, Link.C_EJUICE));
		links.add(new Link("http://irishvape.com/usa-e-juice/e-liquid/pink-spot-vapors/", id, Link.C_EJUICE));
		links.add(new Link("http://irishvape.com/usa-e-juice/suicide-bunny/", id, Link.C_EJUICE));
		links.add(new Link("http://irishvape.com/usa-e-juice/e-liquid/ivape-eliquid/", id, Link.C_EJUICE));
	}
	
	@Override
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		for(Link link : links) {
			try {
				Document doc = Jsoup.connect(link.url).timeout(10000).get();
				Elements productElements = doc.getElementsByClass("product");
				for(Element e : productElements) products.add(getProduct(e, link.category));
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
		return products;
	}
	
	public ProductShop getProduct(Element e, int cat) {
		ProductShop p = new ProductShop();
		
		Element href = e.child(0);
		String l = href.attr("href");
		String t = href.select("h3").get(0).text();
		p.link = l;
		p.name = t;
		p.shop = id;
		p.category = cat;
		p.imageLink = e.select("img").get(0).attr("src");
		Elements priceE = e.getElementsByClass("amount");
		if (priceE.size() > 0) {
			String priceStr;
			if (priceE.get(0).text().split("-").length > 0) priceStr = priceE.get(0).text().split("-")[0].replace(",", ".").replace("�", " ").trim();
			else priceStr = priceE.get(0).text().replace(",", ".").replace("�", " ").trim();
			Double price = Double.valueOf(priceStr);
			p.price = price;
			
		}else{
			p.price = (double) -1;
		}
		System.out.println(p);
		p.shop = id;
		return p;
	}

	@Override
	public ArrayList<Link> getLinks() {
		// TODO Auto-generated method stub
		return null;
	}

}