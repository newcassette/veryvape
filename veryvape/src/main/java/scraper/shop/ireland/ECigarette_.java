package scraper.shop.ireland;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;

public class ECigarette_ implements Shop{

	public final int id = 19;
	private ArrayList<Link> links;
	
	public ECigarette_() {
		links = new ArrayList<Link>();
		
		links.add(new Link("https://www.e-cigarette.ie/starter-kit/", id, Link.C_STARTER_KITS));
	}
	
	@Override
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		for(Link link : links) {
			try {
				Document doc = Jsoup.connect(link.url).userAgent("Chrome").timeout(10000).get();
				Elements productElements = doc.getElementsByClass("product");
				for(Element e : productElements) products.add(getProduct(e, link.category));
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
		return products;
	}
	
	private ProductShop getProduct(Element e, int cat){
		ProductShop p = new ProductShop();
		System.out.println(e.toString());
		return p;
	}
	
	@Override
	public ArrayList<Link> getLinks() {
		// TODO Auto-generated method stub
		return null;
	}

}
