package scraper.shop.ireland;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;

public class ECigShop implements Shop{

	public final int id = 19;
	private ArrayList<Link> links;
	
	public ECigShop() {
		links = new ArrayList<Link>();
		
		links.add(new Link("http://e-cigshop.eu/Electronic-Cigarette-Starter-Kits", id, Link.C_STARTER_KITS));
	}
	
	@Override
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		for(Link link : links) {
			try {
				Document doc = Jsoup.connect(link.url).timeout(10000).get();
				Element productElements = doc.getElementsByClass("product-list").get(0);
				for(Element e:  productElements.children())getProduct(e, link.category);
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
		return products;
	}
	
	private ProductShop getProduct(Element e, int cat){
	//	ProductShop p = new ProductShop();
	//	Element imgElement =  e.getElementsByClass("image");
	//	if(imgElement!=null)imgElement.get(0).select("img").get(0).attr("src");
	//	System.out.println(p.imageLink);
		return null;
	}//
	
	@Override
	public ArrayList<Link> getLinks() {
		// TODO Auto-generated method stub
		return null;
	}

}
