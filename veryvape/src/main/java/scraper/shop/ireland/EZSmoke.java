package scraper.shop.ireland;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;

public class EZSmoke implements Shop{

	ArrayList<Link> links;
	private final int id = 6;
	
	
	public EZSmoke(){
		links = new ArrayList<Link>();
		links.add(new Link("http://www.ezsmoke.ie/14-electronic-cigarette-kits-c-53", id, Link.C_STARTER_KITS));
		links.add(new Link("http://www.ezsmoke.ie/13-ecigarette-parts-c-50", id, Link.C_ACCESSORIES));
		links.add(new Link("http://www.ezsmoke.ie/18-accessories-c-76?id_category=18&n=25", id, Link.C_ACCESSORIES));
		links.add(new Link("http://www.ezsmoke.ie/21-advanced-kits-mods-c-82?id_category=21&n=21", id, Link.C_REGULATED_MODS));
		links.add(new Link("http://www.ezsmoke.ie/23-esmart-c-85", id, Link.C_CIGALIKES));
		links.add(new Link("http://www.ezsmoke.ie/15-ego-ecig-parts?id_category=15&n=27", id, Link.C_CIGALIKES));
		links.add(new Link("http://www.ezsmoke.ie/14-electronic-cigarette-kits-c-53", id, Link.C_CIGALIKES));
		links.add(new Link("http://www.ezsmoke.ie/47-beard-vape-co-eliquids", id, Link.C_EJUICE));
		links.add(new Link("http://www.ezsmoke.ie/37-dekang-10ml-c-487049?id_category=37&n=27", id, Link.C_EJUICE));
		links.add(new Link("http://www.ezsmoke.ie/38-dekang-30ml-c-487057?id_category=38&n=25", id, Link.C_EJUICE));
		links.add(new Link("http://www.ezsmoke.ie/39-dekang-05ml-c-487066?id_category=39&n=26", id, Link.C_EJUICE));
		links.add(new Link("http://www.ezsmoke.ie/31-hazy-days-c-4887?id_category=31&n=45", id, Link.C_EJUICE));
		links.add(new Link("http://www.ezsmoke.ie/26-tjuice-c-4871?id_category=26&n=58", id, Link.C_EJUICE));
		links.add(new Link("http://www.ezsmoke.ie/27-eliquid-sample-packs-c-4875", id, Link.C_EJUICE));
		links.add(new Link("http://www.ezsmoke.ie/30-concentrates-c-4883?id_category=30&n=51", id, Link.C_DIY));
		links.add(new Link("http://www.ezsmoke.ie/42-mod-batteries-chargers?id_category=42&n=13", id, Link.C_BATTERIES));
		links.add(new Link("http://www.ezsmoke.ie/22-pipes-epipes-c-84", id, Link.C_MECH_MODS));
		links.add(new Link("http://www.ezsmoke.ie/46-rebuildable-atomizers-tanks", id, Link.C_RBA));
		links.add(new Link("http://www.ezsmoke.ie/19-wick-wire-c-80?id_category=19&n=14", id, Link.C_DIY));
	}
	
	@Override
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		for(Link link : links) {
			try {
				Document doc = Jsoup.connect(link.url).timeout(10000).get();
				Elements productElements = doc.getElementsByClass("ajax_block_product");
				for(Element e : productElements) products.add(getProduct(e, link.category));
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
		return products;
	}
	
	public ProductShop getProduct(Element e, int cat){
		ProductShop p = new ProductShop();
		
		Element info = e.getElementsByClass("product_img_link").get(0);
		String l = info.attr("href");
		String t = info.attr("title");
		String i = info.getElementsByClass("img-responsive").get(0).attr("src");
		p.link = l;
		p.name = t;
		p.imageLink = i;
		p.category = cat;
		Element priceE = e.getElementsByClass("product-price").get(0);
		Double price = Double.valueOf(priceE.text().replace(",", ".").substring(0, priceE.text().length()-1).trim());
		p.price = price;
		p.shop = id;
		System.out.println(p.toString());
		return p;
	}

	@Override
	public ArrayList<Link> getLinks() {
		// TODO Auto-generated method stub
		return null;
	}

}
