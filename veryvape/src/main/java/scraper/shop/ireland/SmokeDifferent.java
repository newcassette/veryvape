package scraper.shop.ireland;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;

public class SmokeDifferent implements Shop{
	ArrayList<Link> links;
	final int id = 18;

	public SmokeDifferent(){
		links = new ArrayList<Link>();
		
		links.add(new Link("http://smokedifferent.ie/product-category/starter-kits/", id, Link.C_STARTER_KITS));
		links.add(new Link("http://smokedifferent.ie/product-category/liquids/", id, Link.C_EJUICE));
		links.add(new Link("http://smokedifferent.ie/product-category/liquids/page/2/", id, Link.C_EJUICE));
		links.add(new Link("http://smokedifferent.ie/product-category/liquids/page/3/", id, Link.C_EJUICE));
		links.add(new Link("http://smokedifferent.ie/product-category/liquids/page/4/", id, Link.C_EJUICE));
		links.add(new Link("http://smokedifferent.ie/product-category/diy-liquids/", id, Link.C_DIY));
		links.add(new Link("http://smokedifferent.ie/product-category/clearomizers/", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://smokedifferent.ie/product-category/clearomizers/page/2/", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://smokedifferent.ie/product-category/clearomizers/page/3/", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://smokedifferent.ie/product-category/atomizers/", id, Link.C_RBA));
		links.add(new Link("http://smokedifferent.ie/product-category/batteries/", id, Link.C_BATTERIES));
		links.add(new Link("http://smokedifferent.ie/product-category/e-cigarettes/", id, Link.C_REGULATED_MODS));
		links.add(new Link("http://smokedifferent.ie/product-category/e-cigarettes/page/2/", id, Link.C_REGULATED_MODS));
		links.add(new Link("http://smokedifferent.ie/product-category/coils/", id, Link.C_COILS));
		links.add(new Link("http://smokedifferent.ie/product-category/accessories/", id, Link.C_ACCESSORIES));
		links.add(new Link("http://smokedifferent.ie/product-category/accessories/page/2/", id, Link.C_ACCESSORIES));
	}
	
	@Override
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		for(Link link : links) {
			try {
				Document doc = Jsoup.connect(link.url).timeout(10000).get();
				Elements productElements = doc.getElementsByClass("product-small");
				for(Element e : productElements) products.add(getProduct(e, link.category));
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
		return products;
	}
	
	
	public ProductShop getProduct(Element e, int cat){
		ProductShop p = new ProductShop();
		p.link = e.getElementsByClass("inner-wrap").get(0).select("a").get(0).attr("href");
		p.imageLink = e.getElementsByClass("wp-post-image").get(0).attr("src");
		p.name = e.getElementsByClass("name").get(0).text();
		p.price = Double.valueOf(e.getElementsByClass("amount").get(0).text().replace("�", " ").trim());
		p.shop = id;
		p.category = cat;
		System.out.println(p.toString());
		return p;
	}

	@Override
	public ArrayList<Link> getLinks() {
		// TODO Auto-generated method stub
		return null;
	}

}
