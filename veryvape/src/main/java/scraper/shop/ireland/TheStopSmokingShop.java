package scraper.shop.ireland;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;

public class TheStopSmokingShop implements Shop {
	ArrayList<Link> links;
	public final int id = 17;
	
	public TheStopSmokingShop() {
		links = new ArrayList<Link>();
		links.add(new Link("http://thestopsmokingshop.ie/product-category/electronic-cigarettes-for-beginners/", id, Link.C_CIGALIKES));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/advanced-electronic-cigarettes/", id, Link.C_REGULATED_MODS));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/diy-e-liquid/baseliquids/", id, Link.C_DIY));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/batteries-and-chargers/", id, Link.C_REGULATED_MODS));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/batteries-and-chargers/page/2/", id, Link.C_CHARGERS));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/batteries-and-chargers/page/3/", id, Link.C_BATTERIES));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/clearomizer/", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/clearomizer/page/2/", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/clearomizer/page/3/", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/atomizers/", id, Link.C_COILS));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/atomizers/page/2/", id, Link.C_COILS));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/miscellaneous/", id, Link.C_DRIP_TIPS));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/miscellaneous/page/2/", id, Link.C_DRIP_TIPS));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/e-liquid/biofumo/alcoholic-flavors/", id, Link.C_EJUICE));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/e-liquid/biofumo/fruity-flavors-biofumo/", id, Link.C_EJUICE));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/e-liquid/biofumo/mentholated-flavors/", id, Link.C_EJUICE));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/e-liquid/biofumo/other-flavors/", id, Link.C_EJUICE));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/e-liquid/biofumo/tabacco-flavors/", id, Link.C_EJUICE));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/e-liquid/dea/balsamic/", id, Link.C_EJUICE));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/e-liquid/dea/creamy/", id, Link.C_EJUICE));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/e-liquid/dea/floral/", id, Link.C_EJUICE));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/e-liquid/dea/fruity/", id, Link.C_EJUICE));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/e-liquid/dea/tabacco/", id, Link.C_EJUICE));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/diy-e-liquid/baseliquids/", id, Link.C_DIY));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/diy-e-liquid/dks-plus/dks-plus-fruity-flavours/", id, Link.C_DIY));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/diy-e-liquid/dks-plus/dks-plus-specialities/", id, Link.C_DIY));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/diy-e-liquid/dks-plus/dks-plus-specialities/page/2/", id, Link.C_DIY));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/diy-e-liquid/dks-plus/dks-plus-fruity-flavours/page/2/", id, Link.C_DIY));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/diy-e-liquid/dks-plus/dks-plus-tobacco-flavours/", id, Link.C_DIY));
		links.add(new Link("http://thestopsmokingshop.ie/product-category/diy-e-liquid/baseliquids/", id, Link.C_DIY));
	}
	
	
	@Override
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		for(Link link : links) {
			try {
				Document doc = Jsoup.connect(link.url).timeout(10000).get();
				Elements productElements = doc.getElementsByClass("product");
				for(Element e : productElements) products.add(getProduct(e, link.category));
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
		
		return products;
	}
	
	private ProductShop getProduct(Element e, int category){
		ProductShop p = new ProductShop();		
		p.link = e.select("a").get(0).attr("href");
		p.imageLink = e.select("img").get(0).attr("src");
		p.name = e.select("h3").get(0).text();
		String priceStr = e.getElementsByClass("amount").get(0).text();
		if(priceStr.contains("-"))priceStr = priceStr.split("-")[0];
		p.price = Double.valueOf(priceStr.replace("�", " ").trim());
		p.shop = id;
		p.category = category;
		System.out.println(p.toString());
		
		return p;
	}

	@Override
	public ArrayList<Link> getLinks() {
		// TODO Auto-generated method stub
		return null;
	}

}
