package scraper.shop.ireland;

import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;

public class Cigatech implements Shop{
	
	ArrayList<Link> links;
	private final int id = 16;
	
	public Cigatech(){
		links = new ArrayList<Link>();		
		links.add(new Link("http://www.cigatec-ireland.com/17-occasional-smokers-", id, Link.C_CIGALIKES));
		links.add(new Link("http://www.cigatec-ireland.com/18-average-smokers-", id, Link.C_MECH_MODS));
		links.add(new Link("http://www.cigatec-ireland.com/19-heavy-smokers-", id, Link.C_MECH_MODS));
		links.add(new Link("http://www.cigatec-ireland.com/33-batteries-", id, Link.C_CIGALIKES));
		links.add(new Link("http://www.cigatec-ireland.com/34-bottles-and-flasks-", id, Link.C_DIY));
		links.add(new Link("http://www.cigatec-ireland.com/35-cartomizers-", id, Link.C_CARTOMIZER));
		links.add(new Link("http://www.cigatec-ireland.com/40-cases-", id, Link.C_ACCESSORIES));
		links.add(new Link("http://www.cigatec-ireland.com/37-chargers-", id, Link.C_CHARGERS));
		links.add(new Link("http://www.cigatec-ireland.com/38-clearomizers-tanks-and-coils?id_category=38&n=60", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://www.cigatec-ireland.com/39-drip-tips-", id, Link.C_DRIP_TIPS));
		links.add(new Link("http://www.cigatec-ireland.com/25-hybrids-", id, Link.C_MECH_MODS));
		links.add(new Link("http://www.cigatec-ireland.com/26-mechanical-mods-", id, Link.C_MECH_MODS));
		links.add(new Link("http://www.cigatec-ireland.com/27-electronic-mods-?id_category=27&n=24", id, Link.C_REGULATED_MODS));
		links.add(new Link("http://www.cigatec-ireland.com/30-rebuildable-atomizers-?id_category=30&n=24", id, Link.C_RBA));
		links.add(new Link("http://www.cigatec-ireland.com/31-material-and-accessories-?id_category=31&n=24", id, Link.C_DIY));
		links.add(new Link("http://www.cigatec-ireland.com/24-chargers-", id, Link.C_CHARGERS));
		links.add(new Link("http://www.cigatec-ireland.com/43-ahlusion-al-", id, Link.C_EJUICE));
		links.add(new Link("http://www.cigatec-ireland.com/44-alien-visions-", id, Link.C_EJUICE));
		links.add(new Link("http://www.cigatec-ireland.com/65-beard-vape-co", id, Link.C_EJUICE));
		links.add(new Link("http://www.cigatec-ireland.com/56-bordo2-", id, Link.C_EJUICE));
		links.add(new Link("http://www.cigatec-ireland.com/66-cosmic-charlie-s-chalk-dust", id, Link.C_EJUICE));
		links.add(new Link("http://www.cigatec-ireland.com/63-drakes-vapes", id, Link.C_EJUICE));
		links.add(new Link("http://www.cigatec-ireland.com/52-five-pawns-fp-", id, Link.C_EJUICE));
		links.add(new Link("http://www.cigatec-ireland.com/55-flying-vap-", id, Link.C_EJUICE));
		links.add(new Link("http://www.cigatec-ireland.com/51-mido-s-scorpion-blood-", id, Link.C_EJUICE));
		links.add(new Link("http://www.cigatec-ireland.com/54-quack-s-juice-factory-", id, Link.C_EJUICE));
		links.add(new Link("http://www.cigatec-ireland.com/47-sandsmods-ss-", id, Link.C_EJUICE));
		links.add(new Link("http://www.cigatec-ireland.com/53-vaponaute-", id, Link.C_EJUICE));
	}

	@Override
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		for(Link link : links){
			try{
				Document doc = Jsoup.connect(link.url).timeout(10000).get();
				Elements elements = doc.getElementsByClass("product-container");
				for(Element e : elements){
					products.add(getProduct(e, link.category));
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return products;
	}
	
	private ProductShop getProduct(Element e, int category){
		ProductShop p = new ProductShop();
		p.link = e.getElementsByClass("product_img_link").get(0).attr("href");
		p.name = e.getElementsByClass("product_img_link").get(0).attr("title");
		p.imageLink = e.getElementsByClass("img-responsive").get(0).attr("src");
		p.category =category;
		String priceStr = e.getElementsByClass("product-price").get(0).text();
		p.price = Double.valueOf(priceStr.replace("�", " ").replace(",", ".").trim());
		p.shop = id;
		System.out.println(p.toString());
		return p;
	}

	@Override
	public ArrayList<Link> getLinks() {
		// TODO Auto-generated method stub
		return null;
	}

}
