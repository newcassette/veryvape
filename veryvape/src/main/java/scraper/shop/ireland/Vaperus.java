package scraper.shop.ireland;

//COME BACK TO THIS

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;



public class Vaperus implements Shop{
	public ArrayList<Link> links;
	private final int id = 12;
	public Vaperus() {
		links = new ArrayList<Link>();	
		links.add(new Link("http://www.vaperus.ie/55-starter-vaping-kits?n=50&id_category=55", id, Link.C_STARTER_KITS));
		links.add(new Link("http://www.vaperus.ie/7-basic-vaping-kits", id, Link.C_REGULATED_MODS));
		links.add(new Link("http://www.vaperus.ie/8-advanced-vaping-kits", id, Link.C_REGULATED_MODS));
		links.add(new Link("http://www.vaperus.ie/26-disposables", id, Link.C_CIGALIKES));
		links.add(new Link("http://www.vaperus.ie/62-drippers", id, Link.C_RBA));
		links.add(new Link("http://www.vaperus.ie/58-rebuildable", id, Link.C_RBA));
		links.add(new Link("http://www.vaperus.ie/42-mod-tubes", id, Link.C_MECH_MODS));
		links.add(new Link("http://www.vaperus.ie/56-rebuilding-supplies",id, Link.C_DIY));
		links.add(new Link("http://www.vaperus.ie/10-mixology-and-diy", id, Link.C_DIY ));
		links.add(new Link("http://www.vaperus.ie/27-clearomizer", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://www.vaperus.ie/27-clearomizer?p=2", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://www.vaperus.ie/15-atomisers", id, Link.C_RBA));
		links.add(new Link("http://www.vaperus.ie/17-cartomisers", id, Link.C_CARTOMIZER));
		links.add(new Link("http://www.vaperus.ie/21-tanks-accessories", id, Link.C_COILS));
		links.add(new Link("http://www.vaperus.ie/28-coil-wicks", id, Link.C_COILS));
		links.add(new Link("http://www.vaperus.ie/16-batteries-passthroughs", id, Link.C_BATTERIES));
		links.add(new Link("http://www.vaperus.ie/19-chargers", id, Link.C_CHARGERS));
		links.add(new Link("http://www.vaperus.ie/20-drip-tips", id, Link.C_DRIP_TIPS));
		links.add(new Link("http://www.vaperus.ie/14-accessories", id, Link.C_ACCESSORIES));
		links.add(new Link("http://www.vaperus.ie/70-stands-and-storage", id, Link.C_ACCESSORIES));
		links.add(new Link("http://www.vaperus.ie/22-vaperus-e-liquid", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaperus.ie/31-t-juice", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaperus.ie/57-celtic-vapours", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaperus.ie/23-halo-liquids", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaperus.ie/61-dea", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaperus.ie/24-liqua", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaperus.ie/59-joyetech-liquid", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaperus.ie/43-hangsen-", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaperus.ie/25-dekang", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaperus.ie/29-freedom", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaperus.ie/60-vg-liquid", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaperus.ie/77-hypr-tonic", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaperus.ie/78-vaporetto", id, Link.C_EJUICE));
		links.add(new Link("http://www.vaperus.ie/79-decadent", id, Link.C_EJUICE));
	}
	
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		for(Link link : links) {
			try {
				Document doc = Jsoup.connect(link.url).userAgent("Chrome").timeout(10000).get();
				Elements productContainers = doc.getElementsByClass("ajax_block_product");
				for(int i = 0; i < productContainers.size(); i++) {
					products.add(getProduct(productContainers.get(i), link.category));
				}
				
			}catch(IOException e){
				e.printStackTrace();
			}
		}
		return products;
	}
	
	private ProductShop getProduct(Element element, int cat) {		
	
		ProductShop p = new ProductShop();
		p.link = element.getElementsByClass("eb-product-image").select("a").attr("href");
		p.imageLink = element.getElementsByClass("product_img_link").select("img").get(0).attr("src");
		p.name = element.select("h1").text();
		String price = element.select("span").text().replace("�", " ").trim();
		p.price = Double.valueOf(price.substring(price.length()-4, price.length()).trim());
		p.shop = id;
		p.category = cat;
		System.out.println(p.toString());
		return p;	
	}

	public ArrayList<Link> getLinks() {
		// TODO Auto-generated method stub
		return links;
	}

}
