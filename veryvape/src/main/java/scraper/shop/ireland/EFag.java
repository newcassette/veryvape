package scraper.shop.ireland;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;



public class EFag implements Shop{
	public ArrayList<Link> links;
	private final int id = 2;
	public EFag() {
		
		links = new ArrayList<Link>();	
		links.add(new Link("http://www.efag.ie/36-vv-mods", id, Link.C_REGULATED_MODS));
		links.add(new Link("http://www.efag.ie/15-starter-kits", id, Link.C_STARTER_KITS));
		links.add(new Link("http://www.efag.ie/17-e-liquid", id, Link.C_EJUICE));
		links.add(new Link("http://www.efag.ie/17-e-liquid?p=2", id, Link.C_EJUICE));
		links.add(new Link("http://www.efag.ie/18-batteries", id, Link.C_BATTERIES));
		links.add(new Link("http://www.efag.ie/52-chargers", id, Link.C_CHARGERS));
		links.add(new Link("http://www.efag.ie/19-clearomizers", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://www.efag.ie/19-clearomizers?p=2", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://www.efag.ie/42-cartomizers", id, Link.C_CARTOMIZER));
		links.add(new Link("http://www.efag.ie/39-rebuildable-atomizers", id, Link.C_RBA));
		links.add(new Link("http://www.efag.ie/21-accessories", id, Link.C_ACCESSORIES));
		links.add(new Link("http://www.efag.ie/40-diy", id, Link.C_DIY));
	}
	
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		for(Link link : links) {
			try {
				Document doc = Jsoup.connect(link.url).timeout(10000).get();
				Element body = doc.getElementById("product_list");

				Elements productContainers = body.getElementsByClass("ajax_block_product");
				for(int i = 0; i < productContainers.size(); i++) {
					products.add(getProduct(productContainers.get(i), link.category));
				}
				
			}catch(IOException e){
				e.printStackTrace();
			}
		}
		return products;
	}
	
	private ProductShop getProduct(Element element, int cat) {		
		Element imageContainer = element.getElementsByClass("product_img_link").get(0);
		String imageHref = imageContainer.children().get(0).attr("src");
		String name = imageContainer.attr("title");
		double price = Double.valueOf(element.getElementsByClass("price").get(0).text().substring(0, element.getElementsByClass("price").get(0).text().length()-2).replace(",", "."));
		String link = imageContainer.attr("href");
		ProductShop p = new ProductShop();
		p.name = name;
		p.price = price;
		p.link = link;
		p.imageLink = imageHref;
		p.category = cat;
		p.shop = id;
		System.out.println(p);
		return p;
	}

	public ArrayList<Link> getLinks() {
		// TODO Auto-generated method stub
		return links;
	}

}
