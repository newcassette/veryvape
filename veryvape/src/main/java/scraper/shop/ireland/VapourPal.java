package scraper.shop.ireland;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;



public class VapourPal implements Shop{
	public ArrayList<Link> links;
	private final int id = 15;
	public VapourPal() {
		links = new ArrayList<Link>();	
		links.add(new Link("https://www.vapourpal.com/starter-kits.html?limit=all", id, Link.C_STARTER_KITS));
		links.add(new Link("https://www.vapourpal.com/e-liquids.html?limit=all", id, Link.C_EJUICE));
		links.add(new Link("https://www.vapourpal.com/accessories.html?limit=all", id, Link.C_CLEAROMIZERS));
		links.add(new Link("https://www.vapourpal.com/batteries-and-chargers.html?limit=all", id, Link.C_CLEAROMIZERS));
	}
	
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		for(Link link : links) {
			try {
				Document doc = Jsoup.connect(link.url).timeout(10000).get();

				Elements productContainers = doc.getElementsByClass("item");
				for(int i = 0; i < productContainers.size(); i++) {
					products.add(getProduct(productContainers.get(i), Link.C_ACCESSORIES));
				}
				
			}catch(IOException e){
				e.printStackTrace();
			}
		}
		return products;
	}
	
	private ProductShop getProduct(Element element, int cat) {		
		if(element.getElementsByClass("price").text().length() > 0){
			ProductShop p = new ProductShop();	
			p.link = element.getElementsByClass("product-name").select("a").attr("href");
			p.name = element.getElementsByClass("product-name").select("a").text();		
			p.price = Double.valueOf(element.getElementsByClass("price").get(0).text().replace("�", " ").trim());
			p.shop = id;
			p.imageLink = element.select("img").get(0).attr("src");
			p.category = cat;
			System.out.println(p.toString());
			return p;
		}
		return null;
	}

	public ArrayList<Link> getLinks() {
		return links;
	}

}
