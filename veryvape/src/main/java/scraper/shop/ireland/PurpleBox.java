package scraper.shop.ireland;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;



public class PurpleBox implements Shop{
	public ArrayList<Link> links;
	private final int id = 8;
	public PurpleBox() {
		links = new ArrayList<Link>();	
		links.add(new Link("http://purplebox.ie/collections/electronic-cigarette-starter-kits", id, Link.C_STARTER_KITS));	
		links.add(new Link("http://purplebox.ie/collections/electronic-cigarette-starter-kits?page=2", id, Link.C_STARTER_KITS));
		links.add(new Link("http://purplebox.ie/collections/electronic-cigarette-advanced-kits", id, Link.C_REGULATED_MODS));
		links.add(new Link("http://purplebox.ie/collections/electronic-cigarette-advanced-kits?page=2", id, Link.C_REGULATED_MODS));
		links.add(new Link("http://purplebox.ie/collections/e-liquid-ireland", id, Link.C_EJUICE));
		links.add(new Link("http://purplebox.ie/collections/e-liquid-ireland?page=2", id, Link.C_EJUICE));
		links.add(new Link("http://purplebox.ie/collections/e-liquid-ireland?page=3", id, Link.C_EJUICE));
		links.add(new Link("http://purplebox.ie/collections/e-liquid-ireland?page=4", id, Link.C_EJUICE));
		links.add(new Link("http://purplebox.ie/collections/e-liquid-ireland?page=5", id, Link.C_EJUICE));
		links.add(new Link("http://purplebox.ie/collections/e-liquid-ireland?page=6", id, Link.C_EJUICE));
		links.add(new Link("http://purplebox.ie/collections/e-liquid-ireland?page=7", id, Link.C_EJUICE));
		links.add(new Link("http://purplebox.ie/collections/disposable-electronic-cigarettes", id, Link.C_CIGALIKES));
		links.add(new Link("http://purplebox.ie/collections/electronic-cigarette-clearomisers-batteries", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://purplebox.ie/collections/electronic-cigarette-clearomisers-batteries?page=2", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://purplebox.ie/collections/electronic-cigarette-clearomisers-batteries?page=3", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://purplebox.ie/collections/electronic-cigarette-clearomisers-batteries?page=4", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://purplebox.ie/collections/electronic-cigarette-accessories-coils", id, Link.C_ACCESSORIES));
		links.add(new Link("http://purplebox.ie/collections/electronic-cigarette-accessories-coils?page=2", id, Link.C_ACCESSORIES));
		links.add(new Link("http://purplebox.ie/collections/electronic-cigarette-accessories-coils?page=3", id, Link.C_ACCESSORIES));
		links.add(new Link("http://purplebox.ie/collections/electronic-cigarette-accessories-coils?page=4", id, Link.C_ACCESSORIES));
		links.add(new Link("http://purplebox.ie/collections/electronic-cigarette-accessories-coils?page=5", id, Link.C_ACCESSORIES));
	}
	
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		for(Link link : links) {
			try {
				Document doc = Jsoup.connect(link.url).timeout(10000).get();
				Element body = doc.getElementsByClass("products").get(0);

				Elements productContainers = body.getElementsByClass("product");
				for(int i = 0; i < productContainers.size(); i++) {
					products.add(getProduct(productContainers.get(i),Link.C_ACCESSORIES));
				}
				
			}catch(IOException e){
				e.printStackTrace();
			}
		}
		return products;
	}
	
	private ProductShop getProduct(Element element, int cat) {		
		ProductShop p = new ProductShop();
		p.link = element.select("a").attr("href");
		p.link = "http://www.purplebox.ie/"+p.link;
		
		p.name = element.select("img").attr("alt");
		p.shop = id;
		p.category = cat;
		p.imageLink = element.select("img").get(0).attr("src");
		p.price = Double.valueOf(element.select("small").text().replace("�", " ").trim());
		System.out.println(p.toString());
		return p;
	}

	public ArrayList<Link> getLinks() {
		// TODO Auto-generated method stub
		return links;
	}

}
