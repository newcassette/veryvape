package scraper.shop.ireland;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;

public class TheBestShop implements Shop {

	private final int id = 10;
	public ArrayList<Link> links;
	public TheBestShop() {
		links = new ArrayList<Link>();		
		links.add(new Link("http://www.thebestshop.eu/shop/6-electronic-cigarettes-starter-kits",id, Link.C_STARTER_KITS));
		links.add(new Link("http://www.thebestshop.eu/shop/27-vv-mods", id, Link.C_MECH_MODS));
		links.add(new Link("http://www.thebestshop.eu/shop/40-tbs-premium-eliquid", id, Link.C_EJUICE));
		links.add(new Link("http://www.thebestshop.eu/shop/70-diablo-e-liquid", id, Link.C_EJUICE));
		links.add(new Link("http://www.thebestshop.eu/shop/71-ripe-vapes-e-liquid", id, Link.C_EJUICE));
		links.add(new Link("http://www.thebestshop.eu/shop/73-suicide-bunny-e-liquid", id, Link.C_EJUICE));
		links.add(new Link("http://www.thebestshop.eu/shop/74-king-s-crown-e-liquid", id, Link.C_EJUICE));
		links.add(new Link("http://www.thebestshop.eu/shop/72-beard-vape-co-e-liquid", id, Link.C_EJUICE));
		links.add(new Link("http://www.thebestshop.eu/shop/69-lime", id, Link.C_EJUICE));
		links.add(new Link("http://www.thebestshop.eu/shop/61-liqueen-eliquids", id, Link.C_EJUICE));
		links.add(new Link("http://www.thebestshop.eu/shop/62-p1-eliquids", id, Link.C_EJUICE));
		links.add(new Link("http://www.thebestshop.eu/shop/67-provog-e-liquid", id, Link.C_EJUICE));
		links.add(new Link("http://www.thebestshop.eu/shop/68-dr-jack-e-liquid", id, Link.C_EJUICE));
		links.add(new Link("http://www.thebestshop.eu/shop/46-joyetech-eliquids", id, Link.C_EJUICE));
		links.add(new Link("http://www.thebestshop.eu/shop/49-hangsen-eliquid", id, Link.C_EJUICE));
		links.add(new Link("http://www.thebestshop.eu/shop/24-liqua-eliquid", id, Link.C_EJUICE));
		links.add(new Link("http://www.thebestshop.eu/shop/23-standard-eliquid", id, Link.C_EJUICE));
		links.add(new Link("http://www.thebestshop.eu/shop/66-3d-printed-products", id, Link.C_ACCESSORIES));
		links.add(new Link("http://www.thebestshop.eu/shop/28-accessories", id, Link.C_ACCESSORIES));
		links.add(new Link("http://www.thebestshop.eu/shop/29-adapters", id, Link.C_ACCESSORIES));
		links.add(new Link("http://www.thebestshop.eu/shop/42-rebuildable-atomizers", id, Link.C_ACCESSORIES));
		links.add(new Link("http://www.thebestshop.eu/shop/30-atomizers", id, Link.C_COILS));
		links.add(new Link("http://www.thebestshop.eu/shop/31-batteries", id, Link.C_CIGALIKES));
		links.add(new Link("http://www.thebestshop.eu/shop/41-clearomizers", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://www.thebestshop.eu/shop/32-blank-cartomizers", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://www.thebestshop.eu/shop/34-chargers", id, Link.C_CHARGERS));
		links.add(new Link("http://www.thebestshop.eu/shop/35-diy", id, Link.C_DIY));
		links.add(new Link("http://www.thebestshop.eu/shop/36-drip-tips", id, Link.C_DRIP_TIPS));
	}
	
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		for(Link link : links) {
			try {
				Document doc = Jsoup.connect(link.url).timeout(10000).get();
				Element body = doc.getElementById("product_list");

				Elements productContainers = body.getElementsByClass("ajax_block_product");
				for(int i = 0; i < productContainers.size(); i++) {
					products.add(getProduct(productContainers.get(i), link.category));
				}
				
			}catch(IOException e){
				e.printStackTrace();
			}
		}
		return products;
	}
	
	private ProductShop getProduct(Element element, int cat) {		
		Element imageContainer = element.getElementsByClass("product_img_link").get(0);
		String name = imageContainer.attr("title");
		double price = 0;
		if(element.getElementsByClass("price").size()>0)
			price = Double.valueOf(element.getElementsByClass("price").get(0).text().substring(0, element.getElementsByClass("price").get(0).text().length()-2).replace(",", "."));
		ProductShop p = new ProductShop(name, price, id, "");
		p.link = element.select("a").get(0).attr("href");
		p.imageLink = element.select("img").get(0).attr("src");
		p.category = cat;
		System.out.println(p.toString());
		return p;
	}


	public ArrayList<Link> getLinks() {
		return links;
	}

}
