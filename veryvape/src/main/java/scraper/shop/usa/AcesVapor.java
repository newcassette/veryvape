package scraper.shop.usa;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;

public class AcesVapor implements Shop {
	ArrayList<Link> links;
	private static final int id = -1;
	public AcesVapor() {
		links = new ArrayList<Link>();
		links.add(new Link("http://www.acesvapor.com/spades/", id, Link.C_EJUICE));
		links.add(new Link("http://www.acesvapor.com/jokers/", id, Link.C_EJUICE));
		links.add(new Link("http://www.acesvapor.com/hearts-fruity-blends/", id, Link.C_EJUICE));
		links.add(new Link("http://www.acesvapor.com/diamonds-sweet-blends/", id, Link.C_EJUICE));
		links.add(new Link("http://www.acesvapor.com/clubs-tobacco-blends/", id, Link.C_EJUICE));		
	}
	
	@Override
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		for (Link link : links) {
			try {
				// I assume this works because this site's class names are identical to the ones in TwoVaped
				Document doc = Jsoup.connect(link.url).timeout(10000).get();
				Element el = doc.getElementsByClass("ProductList").get(0);
				Elements productElements = el.getElementsByClass("Odd");
				productElements.addAll(el.getElementsByClass("Even"));
				for(Element e : productElements) products.add(getProduct(e, link.category));
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
		return products;
	}
	
	private ProductShop getProduct(Element e, int cat){
		ProductShop p = new ProductShop();
		p.link = e.getElementsByClass("ProductImage").get(0).select("a").get(0).attr("href");
		p.imageLink = e.getElementsByClass("ProductImage").get(0).select("a").get(0).select("img").get(0).attr("href");
		p.name = e.getElementsByClass("ProductDetails").get(0).select("a").get(0).text();
		p.category = cat;
		p.shop = id;
		String priceStr = e.getElementsByClass("ProductPriceRating").get(0).select("em").get(0).text();
		p.price = Double.valueOf(priceStr.replace("$", " ").trim());
		// System.out.println(p.price);
		return p;
	}
	
	@Override
	public ArrayList<Link> getLinks() {
		// TODO Auto-generated method stub
		return null;
	}
}