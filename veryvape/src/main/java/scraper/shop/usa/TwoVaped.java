package scraper.shop.usa;

import java.io.IOException;
import java.sql.Array;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;

public class TwoVaped implements Shop {
	ArrayList<Link> links;
	private static final int id = 21;
	public TwoVaped() {
		links = new ArrayList<Link>();
		links.add(new Link("http://www.2vaped.com/sale/", id, Link.C_MISC));
		links.add(new Link("http://www.2vaped.com/starter-kits/", id, Link.C_STARTER_KITS));
		links.add(new Link("http://www.2vaped.com/sale/?sort=featured&page=2", id, Link.C_MISC));
		links.add(new Link("http://www.2vaped.com/sub-ohm/", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://www.2vaped.com/mods/", id, Link.C_MECH_MODS));
		links.add(new Link("http://www.2vaped.com/mods/?sort=featured&page=2", id, Link.C_MECH_MODS));
		links.add(new Link("http://www.2vaped.com/batteries/", id, Link.C_BATTERIES));
		links.add(new Link("http://www.2vaped.com/rebuildable-dripping-atomizers/", id, Link.C_RBA));
		links.add(new Link("http://www.2vaped.com/rebuildable-dripping-atomizers/?sort=featured&page=2", id, Link.C_RBA));
		links.add(new Link("http://www.2vaped.com/rebuildable-tank-atomizers/", id, Link.C_RBA));
		links.add(new Link("http://www.2vaped.com/tools-wick-wire-and-mesh/", id, Link.C_DIY));
		links.add(new Link("http://www.2vaped.com/clearomizers/", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://www.2vaped.com/clearomizers/?sort=featured&page=2", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://www.2vaped.com/clearomizers/?sort=featured&page=3", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://www.2vaped.com/clearomizers/?sort=featured&page=4", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://www.2vaped.com/cartomizer-tanks/", id, Link.C_CARTOMIZER));
		links.add(new Link("http://www.2vaped.com/drip-tips/", id, Link.C_DRIP_TIPS));
		links.add(new Link("http://www.2vaped.com/accessories/", id, Link.C_ACCESSORIES));
		links.add(new Link("http://www.2vaped.com/huge-e-liquid-selection/", id, Link.C_EJUICE));
		links.add(new Link("http://www.2vaped.com/huge-e-liquid-selection/?sort=featured&page=2", id, Link.C_EJUICE));
		links.add(new Link("http://www.2vaped.com/huge-e-liquid-selection/?sort=featured&page=3", id, Link.C_EJUICE));
		links.add(new Link("http://www.2vaped.com/huge-e-liquid-selection/?sort=featured&page=4", id, Link.C_EJUICE));
		links.add(new Link("http://www.2vaped.com/huge-e-liquid-selection/?sort=featured&page=5", id, Link.C_EJUICE));
		links.add(new Link("http://www.2vaped.com/huge-e-liquid-selection/?sort=featured&page=6", id, Link.C_EJUICE));
	}
	
	@Override
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		for(Link link : links) {
			try {
				Document doc = Jsoup.connect(link.url).timeout(10000).get();
				Element el = doc.getElementsByClass("ProductList").get(0);
				Elements productElements = el.getElementsByClass("Odd");
				productElements.addAll(el.getElementsByClass("Even"));
				for(Element e : productElements) products.add(getProduct(e, link.category));
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
		return products;
	}
	
	private ProductShop getProduct(Element e, int cat){
		ProductShop p = new ProductShop();
		p.link = e.getElementsByClass("ProductImage").get(0).select("a").get(0).attr("href");
		p.imageLink = e.getElementsByClass("ProductImage").get(0).select("a").get(0).select("img").get(0).attr("src");
		p.name = e.getElementsByClass("ProductDetails").get(0).select("a").get(0).text();
		p.category = cat;
		p.shop = id;
		String priceStr = e.getElementsByClass("ProductPriceRating").get(0).select("em").get(0).text();
		if(priceStr.split(" ").length > 1)priceStr = priceStr.split(" ")[1];
		p.price = Double.valueOf(priceStr.replace("$", " ").trim());
		System.out.println(p.toString());
		return p;
	}
	
	@Override
	public ArrayList<Link> getLinks() {
		// TODO Auto-generated method stub
		return null;
	}

}
