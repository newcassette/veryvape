package scraper.shop.usa;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;

public class OneZeroOneVape implements Shop{

	ArrayList<Link> links;
	private final int id = 20; 
	public OneZeroOneVape() {
		links = new ArrayList<Link>();
		links.add(new Link("http://101vape.com/59-fuzion-vapor", id, Link.C_EJUICE));	
		links.add(new Link("http://101vape.com/60-uncle-junk-s", id, Link.C_EJUICE));
		links.add(new Link("http://101vape.com/66-jimmy-the-juice-man", id, Link.C_EJUICE));
		links.add(new Link("http://101vape.com/67-classified", id, Link.C_EJUICE));
		links.add(new Link("http://101vape.com/68-ben-jonson-s", id, Link.C_EJUICE));
		links.add(new Link("http://101vape.com/69-ultimate", id, Link.C_EJUICE));
		links.add(new Link("http://101vape.com/70-9-south-vapes", id, Link.C_EJUICE));
		links.add(new Link("http://101vape.com/17-hot-deals", id, Link.C_RBA));
		links.add(new Link("http://101vape.com/30-mechanical-mods", id, Link.C_MECH_MODS));
		links.add(new Link("http://101vape.com/36-variable-voltagewattage-devices", id, Link.C_REGULATED_MODS));
		links.add(new Link("http://101vape.com/11-rebuildable-atomizers-tanks?id_category=11&n=50", id, Link.C_RBA));
		links.add(new Link("http://101vape.com/11-rebuildable-atomizers-tanks?id_category=11&n=50&p=2", id, Link.C_RBA));
		links.add(new Link("http://101vape.com/40-batteries", id, Link.C_BATTERIES));
		links.add(new Link("http://101vape.com/41-battery-chargers", id, Link.C_CHARGERS));
		links.add(new Link("http://101vape.com/42-battery-cases", id, Link.C_ACCESSORIES));
		links.add(new Link("http://101vape.com/43-battery-safety", id, Link.C_ACCESSORIES));
		links.add(new Link("http://101vape.com/50-meters", id, Link.C_ACCESSORIES));
		links.add(new Link("http://101vape.com/14-clearomizers-glassomizers?id_category=14&n=50", id, Link.C_CLEAROMIZERS));
		links.add(new Link("http://101vape.com/16-drip-tips?id_category=16&n=50", id, Link.C_DRIP_TIPS));
		links.add(new Link("http://101vape.com/16-drip-tips?id_category=16&n=50&p=2", id, Link.C_DRIP_TIPS));
		links.add(new Link("http://101vape.com/15-wick-wire", id, Link.C_DIY));
		links.add(new Link("http://101vape.com/12-accessories-", id, Link.C_ACCESSORIES));
		links.add(new Link("http://101vape.com/53-royal-collection", id, Link.C_MECH_MODS));
		links.add(new Link("http://101vape.com/56-clearance", id, Link.C_RBA));
		links.add(new Link("http://101vape.com/61-replacement-parts", id, Link.C_MISC));
	}
	
	@Override
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		for(Link link : links) {
			try {
				Document doc = Jsoup.connect(link.url).timeout(10000).get();
				Elements productElements = doc.getElementsByClass("ajax_block_product");
				for(Element e : productElements) products.add(getProduct(e, link.category));
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
		return products;
	}
	
	private ProductShop getProduct(Element e, int cat){
		ProductShop p = new ProductShop();
		p.link = e.getElementsByClass("product_img_link").get(0).attr("href");
		p.imageLink = e.getElementsByClass("product_img_link").get(0).select("img").get(0).attr("src");
		p.name = e.select("h3").get(0).text();
		p.category = cat;
		p.shop = id;
		p.price = Double.valueOf(e.getElementsByClass("price").get(0).text().replace("$", " ").trim());
		System.out.println(p.toString());
		return p;
	}

	@Override
	public ArrayList<Link> getLinks() {
		// TODO Auto-generated method stub
		return null;
	}

}
