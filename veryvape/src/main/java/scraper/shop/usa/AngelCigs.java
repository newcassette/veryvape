package scraper.shop.usa;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;

public class AngelCigs implements Shop{

	ArrayList<Link> links;
	private final int id = -1; 
	public AngelCigs() {
		links = new ArrayList<Link>();
		links.add(new Link("http://www.angelcigs.com/mechanical-mods.html", id, Link.C_MECH_MODS));	
		links.add(new Link("http://www.angelcigs.com/mechanical-mods.html?p=2", id, Link.C_MECH_MODS));
		links.add(new Link("http://www.angelcigs.com/mechanical-mods.html?p=3", id, Link.C_MECH_MODS));
		links.add(new Link("http://www.angelcigs.com/mechanical-mods.html?p=4", id, Link.C_MECH_MODS));
		links.add(new Link("http://www.angelcigs.com/mechanical-mods.html?p=5", id, Link.C_MECH_MODS));
		links.add(new Link("http://www.angelcigs.com/rebuildable-electronic-cigarette-atomizer.html", id, Link.C_RBA));
		links.add(new Link("http://www.angelcigs.com/rda.html", id, Link.C_MISC));
		links.add(new Link("http://www.angelcigs.com/rda.html?p=2", id, Link.C_MISC));
		links.add(new Link("http://www.angelcigs.com/rda.html?p=3", id, Link.C_MISC));
		links.add(new Link("http://www.angelcigs.com/rda.html?p=4", id, Link.C_MISC));
		links.add(new Link("http://www.angelcigs.com/rda.html?p=5", id, Link.C_MISC));
		links.add(new Link("http://www.angelcigs.com/vv-vw-mods.html", id, Link.C_MISC));
		links.add(new Link("http://www.angelcigs.com/vv-vw-mods.html?p=2", id, Link.C_MISC));
		links.add(new Link("http://www.angelcigs.com/vv-vw-mods.html?p=3", id, Link.C_MISC));
		links.add(new Link("http://www.angelcigs.com/accessories.html", id, Link.C_ACCESSORIES));
		links.add(new Link("http://www.angelcigs.com/accessories.html?p=2", id, Link.C_ACCESSORIES));
		links.add(new Link("http://www.angelcigs.com/accessories.html?p=3", id, Link.C_ACCESSORIES));
		links.add(new Link("http://www.angelcigs.com/drip-tips.html", id, Link.C_DRIP_TIPS));
		links.add(new Link("http://www.angelcigs.com/drip-tips.html?p=2", id, Link.C_DRIP_TIPS));
		links.add(new Link("http://www.angelcigs.com/drip-tips.html?p=3", id, Link.C_DRIP_TIPS));
		links.add(new Link("http://www.angelcigs.com/drip-tips.html?p=4", id, Link.C_DRIP_TIPS));
		links.add(new Link("http://www.angelcigs.com/clearance.html", id, Link.C_MISC));
		links.add(new Link("http://www.angelcigs.com/clearance.html?p=2", id, Link.C_MISC));
	}
	
	@Override
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		for(Link link : links) {
			try {
				Document doc = Jsoup.connect(link.url).timeout(10000).get();
				Elements productElements = doc.getElementsByClass("item-inner");
				for(Element e : productElements) products.add(getProduct(e, link.category));
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
		return products;
	}
	
	private ProductShop getProduct(Element e, int cat){
		ProductShop p = new ProductShop();
		p.link = e.getElementsByClass("product-name").get(0).select("a").attr("href");
		p.imageLink = e.getElementsByClass("product-image-inner").get(0).select("img").get(0).attr("src");
		p.name = e.getElementsByClass("product-name").get(0).select("a").text().trim();
		p.category = cat;
		p.shop = id;
		p.price = Double.valueOf(e.getElementsByClass("price").get(0).text().replace("$", " ").trim());
		System.out.println(p.price);
		return p;
	}

	@Override
	public ArrayList<Link> getLinks() {
		// TODO Auto-generated method stub
		return null;
	}

}
