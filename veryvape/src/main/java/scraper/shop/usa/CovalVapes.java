package scraper.shop.usa;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;

public class CovalVapes implements Shop {
	ArrayList<Link> links;
	private static final int id = -1;
	public CovalVapes() {
		links = new ArrayList<Link>();
		links.add(new Link("http://covalvapes.com/collections/eliquid", id, Link.C_EJUICE));
		links.add(new Link("http://covalvapes.com/collections/eliquid?page=2", id, Link.C_EJUICE));
		links.add(new Link("http://covalvapes.com/collections/eliquid?page=3", id, Link.C_EJUICE));
		links.add(new Link("http://covalvapes.com/collections/drip-tips", id, Link.C_DRIP_TIPS));
		links.add(new Link("http://covalvapes.com/collections/drip-tips?page=2", id, Link.C_DRIP_TIPS));
		links.add(new Link("http://covalvapes.com/collections/mechanical-mods", id, Link.C_MECH_MODS));
		links.add(new Link("http://covalvapes.com/collections/mechanical-mods?page=2", id, Link.C_MECH_MODS));
		links.add(new Link("http://covalvapes.com/collections/rba", id, Link.C_RBA));
		links.add(new Link("http://covalvapes.com/collections/rba?page=2", id, Link.C_RBA));
		links.add(new Link("http://covalvapes.com/collections/rba?page=3", id, Link.C_RBA));
		links.add(new Link("http://covalvapes.com/collections/resistance-wire", id, Link.C_WICK_WIRE));
		links.add(new Link("http://covalvapes.com/collections/starter-kits", id, Link.C_STARTER_KITS));
		links.add(new Link("http://covalvapes.com/collections/aspire-bvc-coils", id, Link.C_MISC));
		links.add(new Link("http://covalvapes.com/collections/variable-wattage-mods", id, Link.C_MISC));
	}
	
	@Override
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		for (Link link : links) {
			try {
				Document doc = Jsoup.connect(link.url).timeout(10000).get();
				Elements productElements = doc.getElementsByClass("product");
				for(Element e : productElements) products.add(getProduct(e, link.category));
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
		return products;
	}
	
	private ProductShop getProduct(Element e, int cat){
		ProductShop p = new ProductShop();
		p.link = "http://www.covalvapes.com" + e.getElementsByClass("sub").get(0).select("p").get(0).select("a").get(0).attr("href");
		p.imageLink = "http:" + e.getElementsByClass("main").get(0).select("a").get(0).select("img").get(0).attr("src");
		p.name = e.getElementsByClass("sub").get(0).select("p").get(0).select("a").get(0).text();
		p.category = cat;
		p.shop = id;
		String priceStr = e.getElementsByClass("price").get(0).text();
		p.price = Double.valueOf(priceStr.replace("$", " ").trim());
		//System.out.println(p.price);
		return p;
	}
	
	@Override
	public ArrayList<Link> getLinks() {
		// TODO Auto-generated method stub
		return null;
	}
}