package scraper.shop.usa;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import scraper.shop.Link;
import scraper.shop.ProductShop;
import scraper.shop.Shop;

public class ThreeFVape implements Shop {
	ArrayList<Link> links;
	private static final int id = 22;
	
	public ThreeFVape(){
		links = new ArrayList<Link>();
		links.add(new Link("http://www.3fvape.com/39-new-arrival?id_category=39&n=1700", id, Link.C_MISC));
	}
	
	@Override
	public ArrayList<ProductShop> getProducts() {
		ArrayList<ProductShop> products = new ArrayList<ProductShop>();
		for(Link link : links) {
			try {
				Document doc = Jsoup.connect(link.url).timeout(100000).get();
				Elements productElements = doc.getElementsByClass("item");
				for(Element e : productElements) products.add(getProduct(e, link.category));
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
		return products;
	}
	
	private ProductShop getProduct(Element e, int cat){
		ProductShop p = new ProductShop();
		Elements elements = e.getElementsByClass("product-image");
		if(elements.size()>0){
			p.imageLink = elements.get(0).select("img").attr("src");
			p.link = elements.select("a").get(0).attr("href");
		}
		else return null;
		
		p.name = e.getElementsByClass("product-name").select("a").get(0).text();		
		System.out.println(p.name);
		return p;
	}

	@Override
	public ArrayList<Link> getLinks() {
		// TODO Auto-generated method stub
		return null;
	}

}
