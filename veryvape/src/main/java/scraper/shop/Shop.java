package scraper.shop;

import java.util.ArrayList;
import java.util.List;

public interface Shop {
	
	ArrayList<ProductShop> getProducts();
	ArrayList<Link> getLinks();
}
