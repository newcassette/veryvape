package scraper.shop;

public class Link {
	public String url;
	public int shop;
	public int category;
	
	public static final int C_MECH_MODS = 1;
	public static final int C_REGULATED_MODS = 2;
	public static final int C_CHARGERS = 3;
	public static final int C_CIGALIKES = 4;
	public static final int C_BATTERIES = 5;
	public static final int C_COILS = 6;
	public static final int C_DRIP_TIPS = 7;
	public static final int C_CLEAROMIZERS = 8;
	public static final int C_RBA = 9;
	public static final int C_STARTER_KITS = 10;
	public static final int C_WICK_WIRE = 36;
	public static final int C_DIY = 37;
	public static final int C_CARTOMIZER = 38;
	public static final int C_ACCESSORIES = 39;
	public static final int C_EJUICE = 40;
	public static final int C_MISC = 41;
	
	public Link(String url, int shop, int category){
		this.url = url;
		this.shop = shop;
		this.category = category;
	}
}
