package scraper.shop;

public class ProductShop {

	public Double price;
	public String name;
	public String link;
	public int shop;
	public String imageLink;
	public int category;
	
	public ProductShop(String name, Double price, int shop, String link){
		this.name = name;
		this.price = price;
		this.link = link;
		this.shop = shop;
	}
	
	public ProductShop(){}
	
	@Override
	public String toString(){
		String ret = "";
		ret += "Name: " + name + " \n";
		ret += "Price: " + price + " \n";
		ret += "Shop: " + shop + " \n";
		ret += "Link: " + link + " \n";
		ret += "Image: " + imageLink + " \n";
		ret += "Category: " + category + "\n";
		return ret;
	}
}
