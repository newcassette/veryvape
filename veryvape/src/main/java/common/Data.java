package common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONAware;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Data extends TypedHashMap<Object> implements JSONAware {
	private static final long serialVersionUID = -5573063878371862602L;

	public Data() {
		super();
	}

	public Data(String k1, Object v1) {
		super();
		put(k1, v1);
	}
	
	public Data(String k1, Object v1, String k2, Object v2) {
		super();
		put(k1, v1);
		put(k2, v2);
	}
	
	public Data(String k1, Object v1, String k2, Object v2, String k3, Object v3) {
		super();
		put(k1, v1);
		put(k2, v2);
		put(k3, v3);
	}
	
	public Data(String k1, Object v1, String k2, Object v2, String k3, Object v3, Object... args) {
		super();
		put(k1, v1);
		put(k2, v2);
		put(k3, v3);
		for (int i = 0; i < args.length; i += 2) {
			put((String) args[i], args[i + 1]);
		}
	}
	
	public Data(Map<Object, Object> copy) {
		super(copy);
	}

	public static final ContainerFactory containerFactory = new ContainerFactory() {
		@Override
		public List<Object> creatArrayContainer() {
			return new Array();
		}

		@Override
		public Map<Object, Object> createObjectContainer() {
			return new Data();
		}
	};
	
	public static Data readFrom(String string) throws Exception {
		try {
			return (Data) new JSONParser().parse(string, containerFactory);
		} catch (ParseException error) {
			error.printStackTrace();
			throw error;
		}
	}

	public static Data readFrom(Reader reader) throws Exception {
		try {
			return (Data) new JSONParser().parse(reader, containerFactory);
		} catch (IOException error) {
			error.printStackTrace();
			throw error;
		} catch (ParseException error) {
			error.printStackTrace();
			throw error;
		}
	}
	
	public static Data readFrom(File file) throws Exception {
		try {
			Reader reader = new FileReader(file);
			try {
				return readFrom(reader);
			} finally {
				reader.close();
			}
		} catch (FileNotFoundException error) {
			error.printStackTrace();
			throw error;
		}
	}
}