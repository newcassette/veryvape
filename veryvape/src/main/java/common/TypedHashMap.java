package common;


import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

public class TypedHashMap<K> extends LinkedHashMap<K, Object> implements JSONAware {
	private static final long serialVersionUID = -5573063878371862602L;

	public TypedHashMap() {
		super();
	}

	public TypedHashMap(K k1, Object v1) {
		super();
		put(k1, v1);
	}
	
	public TypedHashMap(K k1, Object v1, K k2, Object v2) {
		super();
		put(k1, v1);
		put(k2, v2);
	}
	
	public TypedHashMap(K k1, Object v1, K k2, Object v2, K k3, Object v3) {
		super();
		put(k1, v1);
		put(k2, v2);
		put(k3, v3);
	}
	
	@SuppressWarnings("unchecked")
	public TypedHashMap(K k1, Object v1, K k2, Object v2, K k3, Object v3, Object... args) {
		super();
		put(k1, v1);
		put(k2, v2);
		put(k3, v3);
		for (int i = 0; i < args.length; i += 2) {
			put((K) args[i], args[i + 1]);
		}
	}
	
	public TypedHashMap(Map<K, Object> copy) {
		super(copy);
	}

	public TypedHashMap<K> set(K key, Object value) {
		put(key, value);
		return this;
	}
	
	@SuppressWarnings("unchecked")
	public TypedHashMap<K> set(K key, Object value, Object... args) {
		put(key, value);
		for (int i = 0; i < args.length; i += 2) {
			put((K) args[i], args[i + 1]);
		}
		return this;
	}
	
	public String getString(K key) throws Exception {
		if (!containsKey(key)) throw new Exception();
		return (String) get(key);
	}
	
	public String getString(K key, String missing) {
		if (!containsKey(key)) return missing;
		return (String) get(key);
	}
	
	public int getInt(K key) throws Exception {
		if (!containsKey(key)) throw new Exception();
		return ((Number) get(key)).intValue();
	}
	
	public int getInt(K key, int missing) {
		if (!containsKey(key)) return missing;
		return ((Number) get(key)).intValue();
	}

	public int incr(K key) {
		return (Integer) compute(key, (key0, value) -> {
			if (value instanceof Number) return ((Number) value).intValue() + 1;
			return 1;
		});
	}
	
	public int decr(K key) {
		return (Integer) compute(key, (key0, value) -> {
			if (value instanceof Number) return ((Number) value).intValue() - 1;
			return -1;
		});
	}
	
	public int incr(K key, int increment) {
		return (Integer) compute(key, (key0, value) -> {
			if (value instanceof Number) return ((Number) value).intValue() + increment;
			return increment;
		});
	}
	
	public long getLong(K key) throws Exception {
		if (!containsKey(key)) throw new Exception();
		return ((Number) get(key)).longValue();
	}

	public long getLong(K key, long missing) {
		if (!containsKey(key)) return missing;
		return ((Number) get(key)).longValue();
	}
	
	public float getFloat(K key) throws Exception {
		if (!containsKey(key)) throw new Exception();
		return ((Number) get(key)).floatValue();
	}
	
	public float getFloat(K key, float missing) {
		if (!containsKey(key)) return missing;
		return ((Number) get(key)).floatValue();
	}

	public double getDouble(K key) throws Exception {
		if (!containsKey(key)) throw new Exception();
		return ((Number) get(key)).doubleValue();
	}

	public double getDouble(K key, double missing) {
		if (!containsKey(key)) return missing;
		return ((Number) get(key)).doubleValue();
	}

	public boolean getBoolean(K key) throws Exception {
		if (!containsKey(key)) throw new Exception();
		return (Boolean) get(key);
	}
	
	public boolean getBoolean(K key, boolean missing) {
		if (!containsKey(key)) return missing;
		return (Boolean) get(key);
	}

	public Array getArray(K key) throws Exception {
		if (!containsKey(key)) throw new Exception();
		return (Array) get(key);
	}

	public Data getObject(K key) throws Exception {
		if (!containsKey(key)) throw new Exception();
		return (Data) get(key);
	}
	
	public String toJSONString() {
		return JSONObject.toJSONString(this);
	}
}