package common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Array extends LinkedList<Object> implements JSONAware {
	private static final long serialVersionUID = -6822987497426881200L;

	public Array() {
		super();
	}
	
	public Array(Object... values) {
		super();
		for (Object value : values) add(value); 
	}

	public Array append(Object value) {
		add(value);
		return this;
	}

	public String getString(int index) {
		return (String) get(index);
	}

	public int getInt(int index) {
		return ((Number) get(index)).intValue();
	}
	
	public long getLong(int index) {
		return ((Number) get(index)).longValue();
	}

	public float getFloat(int index) {
		return ((Number) get(index)).floatValue();
	}

	public double getDouble(int index) {
		return ((Number) get(index)).doubleValue();
	}

	public boolean getBoolean(int index) {
		return (Boolean) get(index);
	}

	public Array getArray(int index) {
		return (Array) get(index);
	}

	public Data getObject(int index) {
		return (Data) get(index);
	}

	public Data find(Function<Data, Boolean> predicate) {
		for (Object value : this) {
			if (value instanceof Data && predicate.apply((Data) value)) return (Data) value;
		}
		return null;
	}
	
	public Data find(String key, Object value) {
		for (Object data : this) {
			if (data instanceof Data) {
				if (((Data) data).get(key).equals(value)) return (Data) data;
			}
		}
		return null;
	}
	
	public static Array readFrom(String string) throws Exception {
		try {
			return (Array) new JSONParser().parse(string, Data.containerFactory);
		} catch (ParseException error) {
			error.printStackTrace();
			throw error;
		}
	}

	public static Array readFrom(Reader reader) throws Exception {
		try {
			return (Array) new JSONParser().parse(reader, Data.containerFactory);
		} catch (IOException error) {
			error.printStackTrace();
			throw error;
		} catch (ParseException error) {
			error.printStackTrace();
			throw error;
		}
	}
	
	public static Array readFrom(File file) throws Exception {
		try {
			Reader reader = new FileReader(file);
			try {
				return readFrom(reader);
			} finally {
				reader.close();
			}
		} catch (FileNotFoundException error) {
			error.printStackTrace();
			throw error;
		}
	}
	
	public String toJSONString() {
		return JSONArray.toJSONString(this);
	}
}