define(["Lib", "view/view", "widget/SideBar"], function(_Lib, _View, _SideBar) {

var T = _class(init, _View.T, {
	clearStack  : 	clearStack,
	closePage	: 	closePage,
	addPage     : 	addPage,
	getTopPage	: 	getTopPage,
	hidePage	: 	hidePage,
	showdPage	: 	showPage,
})

function init(t, htmlElement) {
	t.htmlElement = htmlElement;	
	t.stack	= new Array();
	createTopbar(t);
	//createSideBar(t);
}

function createSideBar(t){
	t.sidebar = new _SideBar.T();
	t.htmlElement.appendChild(t.sidebar.htmlElement);
}
function runScript(t){
	alert("called");
}

function createTopbar(t){
	var headerDiv = createElement("div", {"id": "header"});
	t.htmlElement.appendChild(headerDiv);
	var header = createElement("header", {"id": "topBar", "class": "styleBackgroundColor"});
	headerDiv.appendChild(header);
	var container = createElement("div", {"class": "container"});
	header.appendChild(container);
	var pullLeft = createElement("div", {"class": "pull-left fsize13 hide_mobile"})
	container.appendChild(pullLeft);
	var block = createElement("div", {"class": "block text-right"});
	pullLeft.appendChild(block);
	block.appendChild(createElement("a", {"href": "#", "class": "social fa fa-facebook"}));
	block.appendChild(createElement("a", {"href": "#", "class": "social fa fa-twitter"}));
	var barMain = createElement("div", {"class": "barMain"});
	container.appendChild(barMain);
	var nav = createElement("nav", {"class": "nav-second"});
	barMain.appendChild(nav);
	var ul = createElement("ul", {"class": "nav nav-pills nav-second"});
	nav.appendChild(ul);
	ul.appendChild(createElement("li", {"class": "hidden-xs"}, [createElement("a", {"href": "#"},  ["About"])]));
	ul.appendChild(createElement("li", {"class": "hidden-xs"}, [createElement("a", {"href": "#"},  ["Contact"])]));	
	var header2 = createElement("header", {"id": "topNav"});
	headerDiv.appendChild(header2);	
	var container2 = createElement("div", {"class":"container"});

	container2.appendChild(createElement("a", {"class": "logo", "href":"#"},[createElement("img", {"src":"images/logo.png", "height":"40"}), "Ireland's Vaping Index"]));
	header2.appendChild(container2);
	container2.appendChild(createElement("button", {"class": "btn btn-mobile", "data-toggle":"collapse", "data-target": ".nav-main-collapse"}, [createElement("i", {"class": "fa fa-bars"})]));
	/*
	var search = createElement("form", {"class": "search"}, [
                                                           t.searchText = createElement("input", {"type":"text", "class":"form-control", "name":"s","placeholder":"search"}),
                                                           createElement("button", {"class":"fa fa-search"})], {submit: function(e){
                                                           		e.preventDefault();
                                                        	  postRequest("exec", ["product/search", {"search": t.searchText.value, "order": "price"}], function(data) {	
																	var json  = JSON.parse(data.products);
																	emit(t, "search-pressed", json);
																}, function(data) {
																	console.log("fail");
																}, function(prog) {
																	console.log("progress", prog);			
																});
                                                           }});
	container2.appendChild(search);*/
	var navbarCollapse = createElement("div", {"class": "navbar-collapse nav-main-collapse collapse"});
	var navMain = createElement("nav", {"class": "nav-main"});
	navbarCollapse.appendChild(navMain);
	container2.appendChild(navbarCollapse);	
	var topMain = createElement("ul", {"id":"topMain", "class":"nav nav-pills nav-main colored"});
	navMain.appendChild(topMain);
}	

function addPage(t, Page) {
	Page.stackContainer = t;
	if(t.stack.length != 0) {	
		var prevPage = t.stack[t.stack.length - 1];
		prevPage.htmlElement.style.display = "none";	
	}
	t.stack.push(Page);
	Page.htmlElement.style.display = "inherit";
	t.htmlElement.appendChild(Page.htmlElement);
}
function closePage(t) {
	currentPage = t.stack[t.stack.length - 1];
	currentPage.close();
	t.htmlElement.removeChild(currentPage.htmlElement);	
	t.stack.pop();
	if(t.Stack.length != 0) {
		var prevPage = t.stack[t.stack.length - 1];
		prevPage.htmlElement.style.display = "inherit";
	}
}

function showPage() {}
function hidePage() {}

function clearStack(t) {
	t.stack.length = 0;
}
function getTopPage(t) {
	return t.stack[t.stack.length - 1];
}

return {
	T: T,
	init: init,
}
})