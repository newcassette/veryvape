define(["Lib"], function(lib) {

var T = _class(init, null, {
	showSpinner  : showSpinner,
	hideSpinner	 : hideSpinner,
	showDialog	 : showDialog,
	removeDialog : removeDialog,
});

function init(t, htmlElement) {
	t.htmlElement = htmlElement;
	t.spinnerOverlay = createElement("div", {"class": "overlay"}, [
		createElement("div", {"class": "middle spinner-icon"}, [
			createElement("i", {"class": "fa fa-refresh fa-spin"})
		])
	]);
}

function showSpinner(t) {
	t.htmlElement.appendChild(t.spinnerOverlay);
}

function hideSpinner(t) {
	t.htmlElement.removeChild(t.spinnerOverlay);
}

function showDialog(t, titleElement, bodyElement, buttons, arg) {
	var buttonElements = [];
	for (var i = 0; i < buttons.length; ++i) (function(text, callback) {
		var buttonElement = createElement("span", {"class": " paper button"}, [text]);
		buttonElement.onclick = function() {
			if (callback && callback(arg)) return;
			popupElement.remove();
		} 
		buttonElements.push(buttonElement);
	}(buttons[i][0], buttons[i][1]));
	var popupElement = t.popupElement  = createElement("div", {"class": "paper dialog_popup"}, [
			createElement("div", {"class": "main"}, [
				createElement("div", {"class": "title"}, [titleElement]),
				createElement("div", {"class": "content"}, [bodyElement]),
				createElement("div", {"class": "buttons"}, buttonElements)
			])
	]);
	
	t.htmlElement.appendChild(popupElement);
	return popupElement;
}
function removeDialog(t) {
	t.htmlElement.removeChild(t.popupElement);
}
return {
	T: T,
	init: init
};

});