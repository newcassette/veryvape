define(["lib", "view/view"], function(lib, view) {

var T = _class(init, view.T, {
	clearStack  : 	clearStack,
	closePage	: 	closePage,
	addPage     : 	addPage,
	getTopPage	: 	getTopPage,
	hidePage	: 	hidePage,
	showdPage	: 	showPage,
})

function init(t, htmlElement) {
	t.htmlElement = htmlElement;
	
	var pageTitle = createElement("div", {class: "title"});
	var logo = createElement("img", {class: "logo", "src": "css/images/LittleVistaLogoWhite-01.png"});
	var backIcon = t.backIcon = createElement("button", {class: "button"}, [], {click : buttonBack});
	var homeIcon = t.homeIcon = createElement("button", {class: "button"}, [], {click : buttonHome});
	var messagesIcon = t.messagesIcon = createElement("button", {class: "button"}, [], {click : messages});	
	var backImage = createElement("img", {class: "icon", "src": "css/images/backIcon.png"});
	var menuImage = createElement("img", {class: "icon", "src": "css/images/homeIcon.png"});
	var messagesImage = createElement("img", {class: "icon", "src": "css/images/messagesIcon.png"});
	backIcon.appendChild(backImage);
	homeIcon.appendChild(menuImage);
	messagesIcon.appendChild(messagesImage);
	var logoContainer = createElement("div", {class: "logo_container"}, [logo, pageTitle]);
	var iconContainer = createElement("div", {class: "icon_container"}, [
	    backIcon,
	    homeIcon, 
	    messagesIcon,
	    ]);
	
	var topBar = createElement("div", {class: "nav_bar"}, [
		logoContainer,
		iconContainer                                                   
		]);
	
	function buttonChild(){
	 
	}
	
	function messages() {
		
	}
	
	function buttonBack() {
		t.closePage();
	}
	function buttonHome() {
		if(t.Stack[t.Stack.length - 1].title == "Dashboard") {
		}
		if(applicationType=="user"){
			var uMenu =  new userMenu.T();
			t.addPage(uMenu);
		}else if(applicationType=="worker"){
			var sMenu =  new staffMenu.T();
			t.addPage(sMenu);
		}
	
		t.topBar.style.backgroundColor = '#a1dbe0';
	}
	t.topBar = topBar;	
	t.htmlElement.appendChild(t.topBar);
	t.Stack	= new Array();
}

function addPage(t, Page) {
	if(t.Stack.length != 0 && Page.title == "Dashboard" && t.Stack[t.Stack.length - 1].title == "Dashboard") {
		return false;
	}

	Page.stackContainer = t;
	if(t.Stack.length != 0) {	
		var prevPage = t.Stack[t.Stack.length - 1];
		prevPage.htmlElement.style.display = "none";	
	}
	if(t.Stack.length >= 1){
		t.topBar.childNodes[1].childNodes[0].style.visibility = "visible";
	}else {
		t.topBar.childNodes[1].childNodes[0].style.visibility = "hidden";
	}
	t.Stack.push(Page);
	t.topBar.childNodes[0].childNodes[1].setText(Page.title);
	t.topBar.style.backgroundColor = (Page.colour);
	Page.htmlElement.style.display = "inherit";
	t.htmlElement.appendChild(Page.htmlElement);
}
function closePage(t) {
	currentPage = t.Stack[t.Stack.length - 1];
	currentPage.close();
	t.htmlElement.removeChild(currentPage.htmlElement);	
	t.Stack.pop();
	if(t.Stack.length != 0) {
		var prevPage = t.Stack[t.Stack.length - 1];
		prevPage.htmlElement.style.display = "inherit";
	}
	if(t.Stack.length <= 1){
		t.topBar.childNodes[1].childNodes[0].style.visibility = "hidden";
		t.topBar.style.backgroundColor = '#a1dbe0';
	}
	t.topBar.childNodes[0].childNodes[1].setText(prevPage.title);

}
function showPage() {}
function hidePage() {}

function clearStack(t) {
	t.Stack.length = 0;
}
function getTopPage(t) {
	return t.Stack[t.Stack.length - 1];
}

return {
	T: T,
	init: init,
}
})