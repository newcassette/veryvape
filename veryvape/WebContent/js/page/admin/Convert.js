define(["Lib", "page/Page", "data/product"],
	function(_Lib, _Page, _Product) {

	var T = _class(init, _Page.T, {
		});

	function init(t, product) {
		_Page.init(t)
		var brandSelect = createElement("select");

		postRequest("exec", ["brand/list",{}], function(data) {
			var json  = JSON.parse(data.brands);
			for(var i = 0; i < json.length; i++){
				var data = json[i];
				brandSelect.appendChild(createElement("option", {"value": data.id}, [data.name]));
			} 

		}, function(data) {
			console.log("fail");
		}, function(prog) {	

		});

		var categorySelect = createElement("select");
		postRequest("exec", ["category/list", {}], function(data) {
			var json  = JSON.parse(data.categories);
			for(var i = 0; i < json.length; i++) {
				var data = json[i];
				categorySelect.appendChild(createElement("option", {"value": data.id}, [data.name]));
			} 
		}, function(data) {
			console.log("fail");
		}, function(prog) {	

		});

		t.htmlElement.appendChild(createElement("img", {"src":product.list_image}))
		t.htmlElement.appendChild(createElement("h2", {}, [product.name]));
		var f1 = createElement("div", {"class":"col-md-4"});
		var content = createElement("div", {"class":"col-md-4"})
		var f2 = createElement("div", {"class":"col-md-4"});

		t.htmlElement.appendChild(f1);
		t.htmlElement.appendChild(content);
		t.htmlElement.appendChild(f2);

		var brandLabel = createElement("div", {"style": "display:inline"}, ["Brand"]);
		var categoryLabel = createElement("div", {"style": "display:inline"}, ["Category"])

		var brandField = createElement("div", {}, [brandLabel, brandSelect]);
		var categoryField = createElement("div", {}, [categoryLabel, categorySelect]);


		content.appendChild(brandField);
		content.appendChild(categoryField);

		var nameLabel = createElement("div", {}, ["New Name"]);
		var nameInput = createElement("input", {"type": "text", "value":product.name});
		var nameField = createElement("div", {}, [nameLabel, nameInput]);
		var button = createElement("button", {"class":"nomargin btn btn-primary"}, ["Convert"], {click: function(){
			_Product.create(product);
		}});
		t.htmlElement.appendChild(nameField);
		t.htmlElement.appendChild(button);
	}

	return {
		T: T
	}
});