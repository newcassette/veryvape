define(["Lib", "page/Page", "widget/FrontPageItem", "widget/LargeCarousel"], function(_Lib, _Page) {
	
	var T = _class(init, _Page.T, {  
	});
	
	function init(t, product) {
		_Page.init(t);
		
		var tabPanel = createElement("div", {"role": "tabpanel"});
		tabPanel.style.display = "inline-block";
		var li0 = createElement("li", {"role": "presentation", "class": "active"}, [createElement("a", {"href": "#details", "aria-controls":"details", "role":"tab", "data-toggle":"tab"}, [createElement("div", {}, ["Details"])])])
		var li1 = createElement("li", {"role": "presentation"}, [createElement("a", {"href": "#images", "aria-controls":"images", "role":"tab", "data-toggle":"tab"}, [createElement("div", {}, ["Images"])])])
		var li2 = createElement("li", {"role": "presentation"}, [createElement("a", {"href": "#videos", "aria-controls":"videos", "role":"tab", "data-toggle":"tab"}, [createElement("div", {}, ["Videos"])])])
		var li3 = createElement("li", {"role": "presentation"}, [createElement("a", {"href": "#reviews", "aria-controls":"reviews", "role":"tab", "data-toggle":"tab"}, [createElement("div", {}, ["Reviews"])])])
		
		var ul = createElement("ul", {"class":"nav nav-tabs", "role":"tablist"}, [li0, li1, li2, li3]);
		
		var detailsPanel = createElement("div", {"role": "tabpanel", "class": "tab-pane fade in active", "id": "details"}, ["DETAILS"])	
		var imagePanel = createElement("div", {"role": "tabpanel", "class": "tab-pane fade", "id": "images"}, ["IMAGES"])
		var videoPanel = createElement("div", {"role": "tabpanel", "class": "tab-pane fade", "id": "videos"}, ["VIDEOS"])
		var reviewPanel = createElement("div", {"role": "tabpanel", "class": "tab-pane fade", "id": "reviews"}, ["REVIEWS"]);	
		var tabContent = createElement("div", {"class": "tab-content"}, [detailsPanel, imagePanel, videoPanel, reviewPanel]);
		
		tabPanel.appendChild(ul);
		tabPanel.appendChild(tabContent);
		t.htmlElement.appendChild(tabPanel)
		/*
		<div class="tab-content">
		  <div role="tabpanel" class="tab-pane fade in active" id="home">...</div>
		  <div role="tabpanel" class="tab-pane fade" id="profile">...</div>
		  <div role="tabpanel" class="tab-pane fade" id="messages">...</div>
		  <div role="tabpanel" class="tab-pane fade" id="settings">...</div>
		</div>
   */
		$('#myTab a').click(function (e) {
		  e.preventDefault()
		  $(this).tab('show')
		})

	}
    
    return {
        T: T
    }
});