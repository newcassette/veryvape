define(["Lib", "page/Page", "widget/SearchResult", "page/SearchResults"],
		function(_Lib, _Page, _SearchResult, _SearchResults) {
	
	var T = _class(init, _Page.T, { 
		
	});
	
	
	function init(t, stack, json) {
		_Page.init(t, stack);
		t.section = createElement("section");
		t.colmd10 = createElement("div", {"class": "col-md-10"});
		t.htmlElement.appendChild(t.section);
		t.colmd10.appendChild(buildSearch(t));
		createPopularSearches(t);
		createResults(t, json);
		t.stackContainer = stack;
		connect(t.stack, "search-pressed", function(s, json){
			t.colmd10.removeChildren();
			for (var i = 0; i < json.length; i++) {
				var searchResult = new _SearchResult.T(json[i]);
				t.colmd10.appendChild(searchResult.htmlElement);
			}
		});
	}
	
	function buildSearch(t){
		var form = createElement("form", {"class":"clearfix well well-sm search-big"}, [], {submit:function(e) {
			e.preventDefault();
			postRequest("exec", ["product/search", {"search": t.searchText.value, "order": "price"}], function(data) {	
				var json  = JSON.parse(data.products);
				emit(t, "search-pressed", json);
			}, function(data) {
				console.log("fail");
			}, function(prog) {
				console.log("progress", prog);			
			});
			
		}});
		var inputGroup = createElement("div", {"class":"input-group"});
		form.appendChild(inputGroup);
		var search = t.searchText = createElement("input", {"class":"form-control input-lg", "type":"text", "placeholder":"Search"});
		inputGroup.appendChild(search);
		inputGroup.appendChild(createElement("div", {"class":"input-group-btn"}, [createElement("button", {"type":"submit", "class":"btn btn-default input-lg"}, [createElement("i", {"class":"fa fa-fw fa-search fa-lg"})])]));
		form.style.marginTop = "50px";	
		
		connect(t, "search-pressed", function(s, json){
			console.log(json);
			var searchResultsPage = new _SearchResults.T(t.stackContainer, json);
			t.stackContainer.addPage(searchResultsPage);
			//init(t, stack, json)
		});
		
		return form;
	}
	
	function createPopularSearches(t){
		var container = createElement("div", {"class":"col-md-3 col-sm-3"});
		var widget = createElement("div", {"class":"widget"});
		var ul = createElement("ul", {"class":"nav nav-list"});
		container.appendChild(widget);
		widget.appendChild(createElement("h4", {}, ["Popular Searches"]));
		widget.appendChild(ul);
		t.section.appendChild(container);
		ul.appendChild(createElement("li", {}, [createElement("a", {}, ["Atlantis"])]));
		ul.appendChild(createElement("li", {}, [createElement("a", {}, ["Kanger"])]));
		ul.appendChild(createElement("li", {}, [createElement("a", {}, ["Kayfun"])]));
		ul.appendChild(createElement("li", {}, [createElement("a", {}, ["Magma"])]));
		ul.appendChild(createElement("li", {}, [createElement("a", {}, ["Coils"])]));
		ul.appendChild(createElement("li", {}, [createElement("a", {}, ["T-Juice"])]));
		ul.appendChild(createElement("li", {}, [createElement("a", {}, ["Beard"])]));
		ul.appendChild(createElement("li", {}, [createElement("a", {}, ["Nautilus"])]));
			
	}

	function createResults(t, json) {
		var container = createElement("div", {"class": "container"});
		t.section.appendChild(container)
		var row = createElement("row");
		container.appendChild(row);
		
		row.appendChild(t.colmd10);
		for (var i = 0; i < json.length; i++)(function() {
			var searchResult = new _SearchResult.T(json[i]);
			t.colmd10.appendChild(searchResult.htmlElement);
		}) ()
	}

	function createSearchResultBar(t) {
			var section = createElement("section");
	var form = createElement("form", {"class":"clearfix well well-sm search-big"});
	section.appendChild(form);
	t.colmd10.appendChild(section);
	
	var inputGroup = createElement("div", {"class": "input-group"});
	form.appendChild(inputGroup);
	
	var buttonGroup = createElement("div", {"class":"input-group-btn"});
	buttonGroup.appendChild(createElement("button", {"class": "btn btn-default input-lg dropdown-toggle", "data-toggle": "dropdown", "aria-expanded":"false"}, [createElement("span", {"class": "caret"})]));
	var ul = createElement("ul", {"class": "dropdown-menu"});
	ul.appendChild(createElement("li", {"class": "active"}, ["All Countries"]));
	ul.appendChild(createElement("li", {"class": "divider"}));
	ul.appendChild(createElement("li", {}, ["Ireland"]));
	buttonGroup.appendChild(ul);
	
	var search = createElement("input", {"class": "form-control input-lg", "type":"text", "placeholder":"search"});
	inputGroup.appendChild(buttonGroup);
	inputGroup.appendChild(search);
	
	var searchBtn = createElement("div", {"class": "input-group-btn"});
	searchBtn.appendChild(createElement("button", {"type": "submit", "class": "btn btn-default input-lg"}, [createElement("i", {"class": "fa fa-fw fa-search fa-lg"})]));
	inputGroup.appendChild(searchBtn);
	}
	
	return {
		T: T
	}
});