define([], function() {
	
	var T = _class(init, null, {  
		open: open,
		close: close,
		show: show,
		hide: hide	
	});
	function init(t, stack) {
		t.htmlElement = createElement("div", {class : "page_container"});
		t.stack = stack;
	}
	function open(t) {	 
		emit(t, "opened");		
		//add to stack	
	}
	
	function close(t) {
		emit(t, "closed");
		//remove from stack	
	}

	function hide(t) {
		//hide from view	
	}

	function show(t) {
		//show on view
	}
	
	return { 
		
		T: T,
		init: init,
		
	};
	
});


//page.init should take title as a parameter
//when you call page.open put it into one stack
//stack as first parameter in open, close, show, hide