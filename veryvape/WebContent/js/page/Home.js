define(["Lib", "page/Page", "widget/FrontPageItem", "widget/LargeCarousel", "widget/SideBar", "page/Item", "page/SearchResults", "widget/SearchResult"],
		function(_Lib, _Page, _FrontPageItem, _LargeCarousel, _SideBar, _Item, _SearchResults, _SearchResult) {
	
	var T = _class(init, _Page.T, { 
		
	});
	
	
	function init(t) {
		_Page.init(t);
		createDisplayElements(t);
	}

	function createDisplayElements(t) {
		var container = t.container = createElement("div", {"class": "container"});
		t.htmlElement.appendChild(container);
		var row = t.row = createElement("div", {"class": "row", "id":"row"});
		container.appendChild(row);			
		var colmd9 = t.colmd9 = createElement("div", {"class": "col-md-10"});
		
		row.appendChild(buildSearch(t));
		buildNewItems(t);
		var data = {};
	
	}
	
	function buildSearch(t){
		var form = createElement("form", {"class":"clearfix well well-sm search-big"}, [], {submit:function(e) {
			e.preventDefault();
			postRequest("exec", ["product/search", {"search": t.searchText.value, "order": "price"}], function(data) {	
				var json  = JSON.parse(data.products);
				emit(t, "search-pressed", json);
			}, function(data) {
				console.log("fail");
			}, function(prog) {
				console.log("progress", prog);			
			});
			
		}});
		var inputGroup = createElement("div", {"class":"input-group"});
		form.appendChild(inputGroup);
		var search = t.searchText = createElement("input", {"class":"form-control input-lg", "type":"text", "placeholder":"Search"});
		inputGroup.appendChild(search);
		inputGroup.appendChild(createElement("div", {"class":"input-group-btn"}, [createElement("button", {"type":"submit", "class":"btn btn-default input-lg"}, [createElement("i", {"class":"fa fa-fw fa-search fa-lg"})])]));
		form.style.marginTop = "50px";	
		
		connect(t, "search-pressed", function(s, json){
			console.log(json);
			var searchResultsPage = new _SearchResults.T(t.stackContainer, json);
			t.stackContainer.addPage(searchResultsPage);
		});
		
		return form;
	}
	
	function buildNewItems(t) {
		
		var row = createElement("div", {"class": "row margin-top60"});
		t.container.appendChild(row)
		var md3 = createElement("div", {"class": "col-md-2"});
		row.appendChild(md3)
		var ul = createElement("ul", {"class":"side-nav listgroup"});
		md3.appendChild(createElement("strong", {}, ["Popular Searches"]))
		var ul = createElement("ul", {"class":"nav nav-list"});
		md3.appendChild(ul);
		ul.appendChild(createElement("li", {}, [createElement("a", {}, ["Atlantis"])]));
		ul.appendChild(createElement("li", {}, [createElement("a", {}, ["Kanger"])]));
		ul.appendChild(createElement("li", {}, [createElement("a", {}, ["Kayfun"])]));
		ul.appendChild(createElement("li", {}, [createElement("a", {}, ["Magma"])]));
		ul.appendChild(createElement("li", {}, [createElement("a", {}, ["Coils"])]));
		ul.appendChild(createElement("li", {}, [createElement("a", {}, ["T-Juice"])]));
		ul.appendChild(createElement("li", {}, [createElement("a", {}, ["Beard"])]));
		ul.appendChild(createElement("li", {}, [createElement("a", {}, ["Nautilus"])]));
		
		var md9 = createElement("div", {"class": "col-md-10"});
		
		var newRow = createElement("div", {"class": "row"});
		row.appendChild(md9);
		md9.appendChild(createElement("h4", {}, ["New Products"]));
		
		md9.appendChild(newRow)
		postRequest("exec", ["product/newItems",{}], function(data){
			var json  = JSON.parse(data.products);

			for (var i = 0; i < json.length; i++)(function() {
				
				var searchResult = new _SearchResult.T(json[i]);
				md9.appendChild(searchResult.htmlElement);
			}) ()
			
		}, function(data) {
			console.log("fail");
		}, function(prog) {
			console.log("progress", prog);
		})
	}

	return {
		T: T
	}
});