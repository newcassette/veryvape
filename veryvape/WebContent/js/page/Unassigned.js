define(["Lib", "page/Page", "widget/UnassignedProduct"], function(_Lib, _Page, _UnassignedProduct) {
	
	var T = _class(init, _Page.T, {  
	});
	
	function init(t, product) {
		_Page.init(t);
		
		postRequest("exec", ["product/unassigned",{"world": "dfsf"}], function(data) {	
			var json  = JSON.parse(data.products);
			

			var section = createElement("section");
			t.htmlElement.appendChild(section);

			var container = createElement("div", {"class":"container"});
			section.appendChild(container);
			var row = createElement("div", {"class":"row margin-top60"});
			container.appendChild(row);
			var colmd3 = createElement("div", {"class": "col-md-3"});
			row.appendChild(colmd3);
			var colmd9 = createElement("div", {"class": "col-md-9"});
			row.appendChild(colmd9);

			var row2 = createElement("div", {"class": "row"});
			colmd9.appendChild(row2);
			for(var i = 0; i < json.length; i++) {
				var p = new _UnassignedProduct.T(json[i], t.stackContainer);
				row2.appendChild(p.htmlElement);
			}

		}, function(data) {
			console.log("fail");
		}, function(prog) {
			
		});
	}
	
	return {
		T: T
	}
});