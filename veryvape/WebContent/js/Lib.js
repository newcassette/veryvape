function _enum() {
	var properties = {};
	var value = 1;
	for (var i = 0; i < arguments.length; ++i) {
		properties[arguments[i]] = {writable: false, configurable: false, enumerable: true, value: value};
		value *= 2;
	}
	return Object.create(null, properties);
}

function _class(constructor, parent, methods) {
	function _classMethod(methods, method) {
		var actual = methods[method];
		return function() {
			return actual.apply(null, [this].concat(Array.prototype.slice.call(arguments)));
		};
	}
	var base = parent ? parent.prototype : {};
	var properties = {_parent: {value: base, writable: false, enumerable: false}};
	if (methods) for (var method in methods) {
		properties[method] = {value: _classMethod(methods, method), writable: true, enumerable: false};
	}
	constructor = constructor || (parent ? parent.constructor : function() {});
	function Class() { constructor.apply(null, [this].concat(Array.prototype.slice.call(arguments))); }
	Class.prototype = Object.create(base, properties);
	Class.constructor = constructor;
	return Class;
}

Object.defineProperty(Object.prototype, "isEmpty", {
	configurable: false,
	enumerable: false,
	value: function() {
		for (var key in this) return false;
		return true;
	}
});

Element.prototype.remove = function() {
	if (this.parentNode) this.parentNode.removeChild(this);
};

Element.prototype.removeAfterTransition = function() {
	var element = this;
	function remove() {
		element.removeEventListener("webkitTransitionEnd", remove);
		element.removeEventListener("transitionend", remove);
		element.remove();
	}
	element.addEventListener("webkitTransitionEnd", remove);
	element.addEventListener("transitionend", remove);
};

Element.prototype.replace = function(other) {
	if (this.parentNode) this.parentNode.replaceChild(other, this);
};

Element.prototype.removeChildren = function() {
	var child;
	while (child = this.firstChild) this.removeChild(child);
};

Element.prototype.prependChild = function(child) {
	return this.insertBefore(child, this.firstChild);
};

Element.prototype.insertAfter = function(child, after) {
	return this.insertBefore(child, after.nextSibling);
};

Element.prototype.addClass = function(_class) {
	this.classList.add(_class);
};

Element.prototype.removeClass = function(_class) {
	this.classList.remove(_class);
};

Element.prototype.toggleClass = function(_class) {
	this.classList.toggle(_class);
};

Element.prototype.findProperty = function(property) {
	var element = this;
	while (element && !(property in element)) element = element.parentNode;
	return element[property];
};

Element.prototype.setText = function(textFunc) {
	this.textContent = textFunc();
	this.textContent.textFunc = textFunc;
};

Text.prototype.setText = function(textFunc) {
	this.textContent = textFunc();
	this.textFunc = textFunc;
};
Array.prototype.findObj = function (obj, property) {
	var array = this;
	var index = -1;
	for(var i = 0; i < array.length; i++) {	
		//console.log("array item", array[i][property], "obj Item" , obj);
		if( array[i][property] == obj) {
			return index = i;
		}
	} return index; 
};
Array.prototype.find = function (args) {
	var array = this;
	var index = -1;
	if ( array.length == 0) return -1;
	for(var i = 0; i < array.length; i++) {
		if(array[i].id == args.id) {
			return index = i;
		}
	} return index; 
};

if (!Array.prototype.findIndex) {
	Array.prototype.findIndex = function(predicate) {
	if (this == null) {
		throw new TypeError('Array.prototype.findIndex called on null or undefined');
	}
	if (typeof predicate !== 'function') {
		throw new TypeError('predicate must be a function');
	}
    var list = Object(this);
    var length = list.length >>> 0;
    var thisArg = arguments[1];
    var value;

    for (var i = 0; i < length; i++) {
    	value = list[i];
    	if (predicate.call(thisArg, value, i, list)) {
    		return i;
    	}
    }
    return -1;
  };
}

function sprintf(format) {
	var args = Array.prototype.slice.call(arguments, 1);
	if (typeof(format) === "string") {
		return Function.prototype.bind.apply(i18n.sprintf, i18n, [format].concat(args));
	} else {
		return function() {
			return i18n.sprintf.apply(i18n, [format(i18n)].concat(args));
			//Function.prototype.bind.apply(i18n.sprintf, i18n, [format(i18n)].concat(args));
		}
	}
}

function gettext(key) {
	return i18n.gettext.bind(i18n, key);
}

function dgettext(domain, key) {
	return i18n.dgettext.bind(i18b, domain, key);
}

function dcgettext(domain, key, category) {
	return i18n.dcgettext.bind(i18n, domain, key, category);
}

function ngettext(singularKey, pluralKey, value) {
	return i18n.ngettext.bind(i18n, singularKey, pluralKey, value);
}

function dngettext(domain, singularKey, pluralKey, value) {
	return i18n.dngettext.bind(i18n, domain, singularKey, pluralKey, value);
}

function dcngettext(domain, singularKey, pluralKey, value, category) {
	return i18n.dcngettext.bind(i18n, domain, singularKey, pluralKey, value, category);
}

function pgettext(context, key) {
	return i18n.pgettext.bind(i18n, context, key);
}

function dpgettext(domain, context, key) {
	return i18n.dpgettext.bind(i18n, domain, context, key);
}

function npgettext(context, singularKey, pluralKey, value) {
	return i18n.npgettext.bind(i18n, context, singularKey, pluralKey, value);
}

function dnpgettext(domain, context, singularKey, pluralKey, value) {
	return i18n.dnpgettext.bind(i18n, domain, context, singularKey, pluralKey, value);
}

function dcnpgettext(domain, context, singularKey, pluralKey, value, category) {
	return i18n.dcnpgettext.bind(i18n, domain, context, singularKey, pluralKey, value, category);
}

var captureTarget = [], releaseHandler = [];

function capturedMousedown(event) {
	var element = event.target;
	var target = captureTarget[0];
	while (element && element !== target) element = element.parentNode;
	if (!element) {
		event.stopPropagation();
		event.stopImmediatePropagation();
		event.preventDefault();
		releaseHandler[0](event);
	}
}

function capturedClick(event) {
	var element = event.target;
	var target = captureTarget[0];
	while (element && element !== target) element = element.parentNode;
	if (!element) {
		event.stopPropagation();
		event.stopImmediatePropagation();
		event.preventDefault(event);
	}
}

function captureMouse(target, handler) {
	if (!captureTarget.length) document.addEventListener("mousedown", capturedMousedown, true);
	if (!captureTarget.length) document.addEventListener("click", capturedClick, true);
	captureTarget.unshift(target);
	releaseHandler.unshift(handler);
}

function releaseMouse() {
	captureTarget.shift();
	releaseHandler.shift();
	if (!captureTarget.length) document.removeEventListener("click", capturedClick, true);
	if (!captureTarget.length) document.removeEventListener("mousedown", capturedMousedown, true);
}

function captureMouseMove(onmousemove, onmouseup) {
	var timeout = null;
	function capturedMousemove(mouseEvent) {
		mouseEvent.stopPropagation();
		mouseEvent.stopImmediatePropagation();
		mouseEvent.preventDefault();
		if (!timeout) timeout = setTimeout(function() {
			onmousemove(mouseEvent);
			timeout = null;
		}, 1);
	}
	function capturedMouseup(mouseEvent) {
		if (timeout) clearTimeout(timeout);
		mouseEvent.stopPropagation();
		mouseEvent.stopImmediatePropagation();
		mouseEvent.preventDefault();
		document.removeEventListener("mousemove", capturedMousemove, true);
		document.removeEventListener("mouseup", capturedMouseup, true);
		if (onmouseup) onmouseup(mouseEvent);
	}
	document.addEventListener("mousemove", capturedMousemove, true);
	document.addEventListener("mouseup", capturedMouseup, true);
}

function createElement(tag, attrs, children, events) {
	var element = document.createElement(tag);
	if (attrs) for (var attr in attrs) element.setAttribute(attr, attrs[attr]);
	if (children) for (var i = 0; i < children.length; ++i) {
		var child = children[i];
		if (!child) {
			element.appendChild(document.createTextNode(""));
		} else if (typeof child === "function") {
			var textNode = document.createTextNode(child());
			textNode.textFunc = child;
			element.appendChild(textNode);
		} else if (typeof child === "string") {
			element.appendChild(document.createTextNode(child));
		} else {
			element.appendChild(child);
		}
	}
	if (events) for (var event in events) element.addEventListener(event, events[event]);
	return element;
}

function createTextElement(text) {
	return document.createTextNode(text.toString());
}

function createLetterIcon(word, size) {
	var letter = word ? word.substring(0, 1) : "?";
	return createElement("span", {"class": size + " letter-icon letter-" + letter}, [letter]);
}

function connect(object, event, callback) {
	if (!object.eventSystem) {
		Object.defineProperty(object, "eventSystem", {
			configurable: true,
			enumerable: false,
			value: {events: {}}
		});
	}
	var events = object.eventSystem.events;
	var idList = events[event] || [];
	var id = {
		callback: callback,
		args: Array.prototype.slice.call(arguments, 3),
		event: event
	};
	id.disconnect = disconnect.bind(null, object, id);
	idList.push(id);
	events[event] = idList;
	return id;
}

function connectBefore(object, event, callback) {
	if (!object.eventSystem) {
		Object.defineProperty(object, "eventSystem", {
			configurable: true,
			enumerable: false,
			value: {events: {}}
		});
	}
	var events = object.eventSystem.events;
	var idList = events[event] || [];
	var id = {
		callback: callback,
		args: Array.prototype.slice.call(arguments, 3),
		event: event,
		disconnect: disconnect.bind(null, object, id)
	};
	idList.unshift(id);
	events[event] = idList;
	return id;
}

function disconnect(object, id) {
	var eventList = object.eventSystem.events[id.event];
	if (!eventList) return;
	eventList.splice(eventList.indexOf(id), 1);
	if (eventList.length > 0) return;
	delete object.eventSystem.events[id.event];
	if (!object.eventSystem.events.isEmpty()) return;
	delete object.eventSystem;
}

function connectOnce(object, event, callback) {
	var id = connect.apply(connect, arguments);
	id.once = true;
	return id;
}

function emit(object, event) {
	if (!object.eventSystem) return;
	var idList = object.eventSystem.events[event];
	if (!idList) return;
	var i = 0, id;
	while (id = idList[i]) {
		if (id.block) continue;
		var callback = id.callback;
		var stop = callback.apply(callback, id.args.concat(object, Array.prototype.slice.call(arguments, 2)));
		if (id.once) disconnect(object, id);
		if (stop) return true;
		if (idList[i] === id) ++i;
	}
	return false;
}

function connectProperty(object, property, callback) {
	if (!object.propertySystem) {
		Object.defineProperty(object, 'propertySystem', {
			configurable: true,
			enumerable: false,
			value: {
				ids: {},
				properties: {}
			}
		});
	}
	var ids = object.propertySystem.ids;
	var properties = object.propertySystem.properties;
	if (!properties.hasOwnProperty(property)) {
		properties[property] = object[property];
	}
	Object.defineProperty(object, property, {
		configurable: true,
		enumerable: true,
		get: function() { return properties[property] },
		set: function(newValue) {
			var oldValue = properties[property];
			if (oldValue == newValue) return;
			properties[property] = newValue;
			emitProperty(object, property, newValue, oldValue);
		}
	});
	var id = {
		callback: callback,
		property: property,
		args: Array.prototype.slice.call(arguments, 3),
		disconnect: disconnectProperty.bind(null, object, id)
	};
	var idList = ids[property] || [];
	idList.push(id);
	ids[property] = idList;
	return id;
}

function disconnectProperty(object, id) {
	var idList = object.propertySystem.ids[id.property]
	if (!idList) return
	idList.splice(idList.indexOf(id), 1)

	if (idList.length > 0) return
	delete object[id.property]
	object[id.property] = object.propertySystem.properties[id.property]
	delete object.propertySystem.ids[id.property]
	delete object.propertySystem.properties[id.property]

	if (Object.keys(object.propertySystem.ids).length > 0) return
	delete object.propertySystem
}

function emitProperty(object, property) {
	if (!object.propertySystem) return false;
	var idList = object.propertySystem.ids[property];
	if (!idList) return false;
	var length = idList.length;
	var i = 0, id;
	while (id = idList[i]) {
		var callback = id.callback;
		var value = object[property];
		if (!id.hasOwnProperty('compFunc') || id.compFunc(value)) {
			var stop = callback.apply(callback, id.args.concat(object, Array.prototype.slice.call(arguments, 2)));
			if (id.once) disconnectProperty(object, id);
			if (stop) return true;
		}
		if (id == idList[i]) ++i;
	}
	return false;
}

function connectPropertyOnce(object, property, callback) {
	var id = connectProperty.apply(connectProperty, arguments)
	id.once = true
	return id
}

function whenPropertyDefined(object, property, callback) {
	if (object.hasOwnProperty(property) && object[property] !== undefined) {
		callback.apply(callback, [object].concat(Array.prototype.slice.call(arguments, 3)).concat([object[property]]))
		return
	}
	return connectPropertyOnce.apply(connectPropertyOnce, arguments)
}

function whenPropertySatisfies(object, property, compFunc, callback) {
	if (compFunc(object[property])) {
		callback.apply(callback, [object].concat(Array.prototype.slice.call(arguments, 4)).concat([object[property]]))
		return
	}
	var args = [object, property, callback].concat(Array.prototype.slice.call(arguments, 4))
	var id = connectPropertyOnce.apply(connectPropertyOnce, args)
	id.compFunc = compFunc
	return id
}

function whenPropertyIs(object, property, value, callback) {
	function test(a, b) { return a === b; }
	whenPropertySatisfies(object, property, test.bind(null, value), callback);
}

function whenPropertyIsNot(object, property, value, callback) {
	function test(a, b) { return a !== b; }
	whenPropertySatisfies(object, property, test.bind(null, value), callback);
}

function whenPropertyIsNull(object, property, callback) {
	whenPropertyIs(object, property, null, callback)
}

function whenPropertyIsNotNull(object, property, callback) {
	whenPropertyIsNot(object, property, null, callback)
}

function whenPropertyAndMask(object, property, mask, callback) {
	function test(a, b) { return a & b; }
	whenPropertySatisfies(object, property, test.bind(null, mask), callback);
}

function whenPropertyNotAndMask(object, property, mask, callback) {
	function test(a, b) { return !(a & b); }
	whenPropertySatisfies(object, property, test.bind(null, mask), callback);
}

function postRequest(url, data, success, failure, progress) {
	var request = new XMLHttpRequest()
	request.onload = function(event) {
		switch (this.status) {
		case 200: return success(JSON.parse(this.responseText))
		case 501:
		case 504: return failure(this.status, {})
		default: return failure(this.status, JSON.parse(this.responseText || "{}"))
		}
	}
	request.onerror = function(event) {
		failure(-1)
	}
	if (progress) request.upload.onprogress = function(event) {
		progress(event.loaded, event.total)
	}
	request.open("POST", url, true)
	request.setRequestHeader("Content-Type", "application/json")
	request.send(JSON.stringify(data))
}

function requestSuccess() {}
function requestFailure(xhr, status) { console.log('ERROR: request', xhr, status) }

function request(method, args, success, failure, progress) {
	var data = {}
	for (var arg in args) data[arg] = args[arg]
	postRequest("exec", [method, data],
		success || function() {},
		failure || function(status, result) { console.log('ERROR: request', status, result) },
		progress
	)
}

function subscribe(event, callback) {
	//
}

// For testing requests in the console
function qrequest(method, args) {
	request(method, args, function(result) {
		console.log(result)
	})
}

function registerEventHandler(event, handler) {
	window.eventsFeed.addEventListener(event, function(event) {
		handler(JSON.parse(event.data))
	})
}
