
define(["Lib", "view/stack", "page/Home", "page/Item", "page/NewProduct", "page/Unassigned", "page/admin/Convert", "page/SearchResults"], 
	function(_Lib, _Stack, _Home, _Item, _NewProduct, _Unassigned, _Convert, _SearchResults) {
	var body = document.body;	


	body.addClass("smoothscroll");
	var viewElement = createElement("div", {"id":"wrapper"});
	body.appendChild(viewElement);
	window.app = {};
	var mainStack = new _Stack.T(viewElement);
	window.app.stack = mainStack;
	mainStack.addPage(new _Home.T(mainStack));
	//mainStack.addPage(new _SearchResults.T())
})
