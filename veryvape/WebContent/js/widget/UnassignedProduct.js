define(["Lib", "page/admin/Convert"], 
	function(_Lib, _Convert) {
	
	var T = _class(init, null, {  

	});
    
	function init(t, product, sc) {
		t.stackContainer = sc;
		t.htmlElement = createElement("div", {"class": "col-md-4 col-sm-3"});
		var a = createElement("a", {"class":"shop-item-list"});
		t.htmlElement.appendChild(a);
		var figure = createElement("figure");
		a.appendChild(figure);
		figure.appendChild(createElement("img", {"class":"img-responsive", "src":product.list_image}));

		/*
										div class="product-info"><!-- title and price -->
											<h2>
												<span class="product-name">Lorem ipsum dolor</span>
												<span class="bold">$90</span> <span class="line-through">$120</span>
											</h2>
										</div><!-- /title and price -->

		*/

		var info = createElement("div", {"class": "product-info"});
		var h2 = createElement("h2");
		info.appendChild(h2);
		h2.appendChild(createElement("span", {"class": "product-name"}, [product.name]));
		a.appendChild(info);

		t.htmlElement.onclick = function() {
			t.stackContainer.addPage(new _Convert.T(product));
		}
	}

	return { 
		T: T
	}
});