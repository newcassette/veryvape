define(["Lib"], function(_Lib) {
	
	var T = _class(init, null, {  
	});
    
    /***
     *product = {"name": "ccc",
     *           "image: "ccc",
     *           "price": "ccc"}
     *           "description": "ccc"}
     *              "reviewNumber": "ccc:}
    */
    function init(t, product) {
        t.htmlElement = createElement("div", {"class": "list-group-item"});                                  
        t.product = product;
        
        var thumb = createElement("div", {"class": ""});
        t.htmlElement.appendChild(thumb);
        
        var image = createElement("img", {"src": t.product.image}, [], {click: function(){
            emit(t, "product-clicked");
        }})
        var caption = createElement("div", {"class": "caption"});
        var price = createElement("h4", {"class": "pull-right"}, [t.product.price], {click: function() {
            emit(t, "product-clicked");
        }});
        var productName = createElement("h4");
        productName.appendChild(createElement("a", {"href":"#"}, [t.product.name] , {click: function() {
            emit(t, "product-clicked");
        }}));
        var description = createElement("p", {}, [t.product.description]);
        caption.appendChild(price);
        caption.appendChild(productName)
        caption.appendChild(description);
        
        thumb.appendChild(image);
        thumb.appendChild(caption);
        
        var ratings = createElement("div", {"class": "ratings"});
        var pullRight = createElement("p", {"class": "pull-right"}, [t.product.reviewNumber]);
        var stars = createElement("p");
        for (var j = 0; j < 5; j++) stars.appendChild(createElement("span", {"class": "glyphicon glyphicon-star"}));
        ratings.appendChild(pullRight);
        ratings.appendChild(stars);
    }
    
    return {
        T: T
    }
});