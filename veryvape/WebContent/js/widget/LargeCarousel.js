define(["Lib"], function(_Lib) {
	
	var T = _class(init, null, {  
	});
    
    function init(t, products){
        t.htmlElement = createElement("div", {"class": "row carousel-holder"});
        var colmd12 = createElement("div", {"class": "col-md-12"});
        t.htmlElement.appendChild(colmd12);
        var generic = createElement("div", {"id": "carousel-example-generic", "class": "carousel slide", "data-ride": "carousel"});
        colmd12.appendChild(generic);
        var indicators = createElement("ol", {"class": "carousel-indicators"});
        generic.appendChild(indicators);
        for (var i = 0; i < products.length; i++) {
            var indicator = createElement("li",  {"data-target": "#carousel-example-generic", "data-slide-to":i.toString()})
            if (i == 0) {
                indicator.addClass("active");
            }
            indicators.appendChild(indicator);
        }		
        var inner = createElement("div", {"class": "carousel-inner", "role": "listbox"});
        generic.appendChild(inner);		
        for (var i = 0; i < products.length; i++){
            var item = createElement("div", {"class": "item"});
            var innerItem = createElement("img", {"class": "slide-image", "src": "http://irishvape.com/wp-content/uploads/2014/04/aspire-Nautilus.jpg"});
            item.appendChild(innerItem);
            if (i == 0) {
                item.addClass("active");
            }
            inner.appendChild(item);
        }		
        //ADD INDICATORS
        var left = createElement("a", {"class": "left carousel-control", "href": "#carousel-example-generic", "role": "button", "data-slide": "prev"})
        left.appendChild(createElement("span", {"class": "glyphicon glyphicon-chevron-left"}));
        left.appendChild(createElement("span", {"class": "sr-only"}));	
        var right = createElement("a", {"class": "right carousel-control", "href": "#carousel-example-generic", "data-slide": "next"})
        right.appendChild(createElement("span", {"class": "glyphicon glyphicon-chevron-right"}));					  
        generic.appendChild(left);
        generic.appendChild(right);
    }
    
    return{
        T: T
    }
    
});